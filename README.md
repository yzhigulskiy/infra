# Automated Crystal infrastructure setup

### Software Prerequisite:
- terraform:
```(Terraform v0.13.02)```
- gcloud:
    ```Google Cloud SDK 286.0.0
    alpha 2020.03.24
    beta 2020.03.24
    bq 2.0.55
    core 2020.03.24
    gsutil 4.48
    kubectl 2020.03.24
  ```
- python3
- virtualenv
- helm3
- helm plugin for gcs
  `helm plugin install https://github.com/hayorov/helm-gcs.git`

### Usage:
- Manually create new bucket for terraform state, with name `tf-state-"PROJECT_ID"`:
ex: `tf-state-igenius-terraform-playground-1`
- Manually create secret gcp-registry
- Install helm: https://helm.sh/docs/intro/install/
- Setup appropriate GCP Project: 
```
gcloud config set project igenius-terraform-playground-1
gcloud auth application-default login
gcloud auth configure-docker
```

### Python virtualenv:
```
ls
docker  init.py  projects  python_terraform  README.md  requirements.txt  templates  terraform  tf.log
# # # # #
python3 -m venv env
source env/bin/activate
python3 -m pip install -r requirements.txt
```

### Prepare project structure:
Inside `projects` directory, create a directory with the name of you GCP project
```
gcloud config list | grep project
    project = igenius-terraform-playground-1
mkdir -p project/igenius-terraform-playground-1/tenants
```
Read about project json configuration from `project/README.md`

Tenant installation split into phases:
1. Installing infrastructure (network, k8s and infra modules)
2. Installing tenant. 

#### 1. Installing infrastructure
```
python3 init.py -p igenius-terraform-playground-1 -a apply
```

#### 2. Install tenant:
Run tenant installation script
```
python3 init.py -p igenius-terraform-playground-1 -t tenant1 -a apply
```

#### Destroying tenant:
```
python3 init.py -p igenius-terraform-playground-1 -t tenant1 -a destroy
```

#### Destroying project:
```
python3 init.py -p igenius-terraform-playground-1 -a destroy
```

#### Get TF configuration for network, k8s, infra and crystal tenant.
```
python3 init.py -p igenius-terraform-playground-1 -t tenant1 -c
```

##### New user into already existing tenant.
```
# Add user to templates: "templates/crystal-terraform.tfvars", "template/kubernetes-terraform.tfvars"
# Generate a configs:
python3 init.py -p igenius-private-betas -t mais -c
cd terraform/crystal/igenius-private-betas/mais
terraform apply -target=module.tenant.module.cloudsql.data.google_secret_manager_secret_version.secret
terraform apply -target=module.tenant.module.sql-provision.data.google_secret_manager_secret_version.secret
python3 init.py -p igenius-private-betas -t mais -a apply
```

##### Updating SQL Provisioning.
```
# Generate a configs:
python3 init.py -p igenius-private-betas -t mais -c
cd terraform/crystal/igenius-private-betas/mais
terraform init
terraform taint module.tenant.module.sql-provision.helm_release.crystal-sql-provision
python3 init.py -p igenius-private-betas -t mais -a apply
```

