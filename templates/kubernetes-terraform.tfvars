REGION        = "{{ REGION }}"
PROJECT       = "{{ PROJECT }}"
zone          = "{{ zone }}"
network_id    = "projects/{{ PROJECT }}/global/networks/{{ network_id }}"
gke_version           = "{{ gke_version }}"
gke_cluster_name      = "{{ gke_cluster_name }}"
gke_pool_machine_type = "{{ gke_pool_machine_type }}"
gke_pool_name         = "{{ gke_pool_name }}"
gke_pool_node_count   = "{{ gke_pool_node_count }}"
gcp_registry_username = "{{ gcp_registry_username }}"
gke_admin_user        = "{{ gke_admin_user }}"
max_node_count        = "{{max_node_count}}"
min_node_count        = "{{min_node_count}}"
node_version          = "{{node_version}}"
master_authorized_networks = [
  {display_name = "net1", cidr_block = "34.244.125.6/32"},
  {display_name = "net2", cidr_block = "93.51.191.40/29"},
  {display_name = "net3", cidr_block = "2.230.199.69/32"},
  {display_name = "net4", cidr_block = "35.187.45.191/32"},
  {display_name = "net5", cidr_block = "35.241.236.71/32"},
  {display_name = "net6", cidr_block = "35.187.172.174/32"},
  {display_name = "net7", cidr_block = "104.155.34.81/32"}
]
users = [
  {name = "tenant-owner"},
  {name = "sqitch"},
  {name = "ketoprov"},
  {name = "postgres"},
  {name = "login-manager"},
  {name = "identity-provider-adapter-local"},
  {name = "permissions-service"},
  {name = "topic-service"},
  {name = "louvre"},
  {name = "nats"},
  {name = "advisor-manager"},
  {name = "hydraprov"},
  {name = "alerting-squad"},
  {name = "alerting-scheduler"},
  {name = "nlp-entities-manager"},
  {name = "system-settings"},
  {name = "notification-center"},
  {name = "train-task-manager"}
]

