terraform {
  backend "gcs" {
    bucket = "tf-state-{{ PROJECT }}"
    prefix = "{{ backend }}"
  }
  required_providers {
    helm = {
      source = "hashicorp/helm"
      version = "1.3.0"
    }
  }
}