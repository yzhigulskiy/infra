The tenant configuration is split between 3 json files:
- projects/PROJECT_ID/project.json
- projects/PROJECT_ID/tenants/TENANT_NAME.json
- projects/global-service-version.json

Example of project.json 
```
{
  "PROJECT": "PROJECT_ID",
  "backend": "tf-state-PROJECT_ID",
  "REGION": "europe-west3",
  "zone": "europe-west3",
  "network_id": "network-1",
  "gcp_registry_username": "gcp-registry",
  "modules": {
    "network": {
      "backend": "network/state"
    },
    "kubernetes": {
      "backend": "kubernetes/state",
      "gke_version": "1.16.13-gke.401",
      "gke_cluster_name": "cluster-1",
      "gke_pool_name": "pool-2-8",
      "gke_pool_machine_type": "n1-standard-2",
      "gke_pool_node_count": 1,
      "gcp_registry_username": "gcp-registry",
      "gke_admin_user": "admin"
    },
    "infra": {
      "backend": "infra/state"
    }
  }
}
```
Most important is a kubernetes section.
Before make a new installation think about tenant numbers.
Provided configuration is for 2 tenants without load. Change ```gke_pool_machine_type``` or ```gke_pool_node_count``` if you are going to install more tenants.


Example of TENANT_NAME.json
```
{
  "name": "TENANT_NAME",
  "PROJECT": "PROJECT_ID",
  "backend": "crystal/TENANT_NAME/state",
  "cloudsql_tier": "db-custom-1-3840",
  "tenant_base_endpoint": "TENANT_NAME.crystal.ai",
  "dataset_template_bucket": "igenius-argo-TENANT_NAME",
  "tenant_minio_endpoint": "nlp-minio-TENANT_NAME",
  "retrain_nats_endpoint": "nats://train-crystal-nats.gcp.crystal.ai:4222",
  "crystal_advisor_manager": "v3.6.3"
}
```
For new tenant need to replace TENANT_NAME and PROJECT_ID by real tenant name and project id.

You could put inside a tenant json configuration any section from projects/global-service-version.json file. It would take higher priority for this tenant that version in global file.
You could see a "crystal_advisor_manager" section as an example.
