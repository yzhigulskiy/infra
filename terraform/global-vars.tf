variable "REGION" {}
variable "PROJECT" {}
variable "zone" {}

provider "google" {
  project = var.PROJECT
  region  = var.REGION
  zone    = var.zone
}