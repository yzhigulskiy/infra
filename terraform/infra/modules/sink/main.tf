// Logging sink
resource "google_logging_project_sink" "logging-sink" {
  name                   = "logging_sink"
  destination            = "storage.googleapis.com/${var.logging_sink_bucket_destination}"
  filter                 = var.logging_sink_filter
  unique_writer_identity = true
}

resource "google_project_iam_binding" "log-writer" {
  role    = "roles/storage.objectCreator"
  project = var.sink_project_id
  members = [
    google_logging_project_sink.logging-sink.writer_identity,
  ]
}

// Trace sink
resource "null_resource" "trace_sink" {
  provisioner "local-exec" {
    command = "gcloud alpha trace sinks create trace_sink bigquery.googleapis.com/projects/${var.sink_project_number}/datasets/${var.trace_sink_dataset}"
  }
}

data "external" "get_trace_sink_id" {
  depends_on = [null_resource.trace_sink]
  program    = ["bash", "${path.module}/get_trace_sink_id.sh"]
}

resource "google_project_iam_binding" "trace-writer" {
  depends_on = [data.external.get_trace_sink_id]
  role       = "roles/bigquery.dataEditor"
  project    = var.sink_project_id
  members = [
    "serviceAccount:${data.external.get_trace_sink_id.result.writer_identity}",
  ]
}

resource "null_resource" "destroy_trace_sink" {
  provisioner "local-exec" {
    when    = destroy
    command = "yes | gcloud alpha trace sinks delete trace_sink"
  }
}