locals {
  dockerconfigjson = {
    "auths" : {
      "https://eu.gcr.io" = {
        email    = "youremail@example.com"
        username = "_json_key"
        auth     = "${var.GCP_REGISTRY_USER}"
      }
    }
  }
}

resource "kubernetes_namespace" "namespace" {
  metadata {
    annotations = {
      name = "nats-io"
    }
    name = "nats-io"
  }
}

resource "kubernetes_secret" "gcp-registry" {
  depends_on = [
    kubernetes_namespace.namespace
  ]
  metadata {
    name        = var.gcp_registry_username
    namespace   = "nats-io"
    annotations = {}
  }
  data = {
    ".dockerconfigjson" = jsonencode(local.dockerconfigjson)
  }
  type = "kubernetes.io/dockerconfigjson"
}

resource "kubernetes_default_service_account" "default" {
  depends_on = [
    kubernetes_secret.gcp-registry
  ]
  metadata {
    namespace = "nats-io"
  }
  image_pull_secret {
    name = "gcp-registry"
  }
}

resource "helm_release" "nats-streaming-operator" {
  name      = "nats-streaming-operator"
  chart     = "../helm/nats-streaming-operator"
  namespace = "nats-io"
}

