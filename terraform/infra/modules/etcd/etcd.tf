locals {
  dockerconfigjson = {
    "auths" : {
      "https://eu.gcr.io" = {
        email    = "youremail@example.com"
        username = "_json_key"
        auth     = "${var.GCP_REGISTRY_USER}"
      }
    }
  }
}

resource "kubernetes_namespace" "namespace" {
  metadata {
    annotations = {
      name = "etcd"
    }
    name = "etcd"
  }
}

resource "kubernetes_secret" "gcp-registry" {
  depends_on = [
    kubernetes_namespace.namespace
  ]
  metadata {
    name        = var.gcp_registry_username
    namespace   = "etcd"
    annotations = {}
  }
  data = {
    ".dockerconfigjson" = jsonencode(local.dockerconfigjson)
  }
  type = "kubernetes.io/dockerconfigjson"
}

resource "kubernetes_default_service_account" "default" {
  depends_on = [
    kubernetes_secret.gcp-registry
  ]
  metadata {
    namespace = "etcd"
  }
  image_pull_secret {
    name = "gcp-registry"
  }
}

resource "helm_release" "etcd-operator" {
  name       = "etcd-operator"
  chart      = "etcd-operator"
  repository = "../helm"
  namespace  = "etcd"
}

