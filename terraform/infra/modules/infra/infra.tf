locals {
  dockerconfigjson = {
    "auths" : {
      "https://eu.gcr.io" = {
        email    = "youremail@example.com"
        username = "_json_key"
        auth     = "${var.GCP_REGISTRY_USER}"
      }
    }
  }
}

resource "kubernetes_namespace" "namespace" {
  metadata {
    annotations = {
      name = "infrustructure-provision"
    }
    name = "infrustructure-provision"
  }
}

resource "kubernetes_secret" "gcp-registry" {
  depends_on = [
    kubernetes_namespace.namespace
  ]
  metadata {
    name        = var.gcp_registry_username
    namespace   = "infrustructure-provision"
    annotations = {}
  }
  data = {
    ".dockerconfigjson" = jsonencode(local.dockerconfigjson)
  }
  type = "kubernetes.io/dockerconfigjson"
}

resource "kubernetes_secret" "gcp-registry-default" {
  depends_on = [
    kubernetes_namespace.namespace
  ]
  metadata {
    name        = var.gcp_registry_username
    namespace   = "default"
    annotations = {}
  }
  data = {
    ".dockerconfigjson" = jsonencode(local.dockerconfigjson)
  }
  type = "kubernetes.io/dockerconfigjson"
}

resource "kubernetes_default_service_account" "default-infrustructure-provision" {
  depends_on = [
    kubernetes_secret.gcp-registry
  ]
  metadata {
    namespace = "infrustructure-provision"
  }
  image_pull_secret {
    name = "gcp-registry"
  }
}

resource "kubernetes_default_service_account" "default-default" {
  depends_on = [
    kubernetes_secret.gcp-registry
  ]
  metadata {
    namespace = "default"
  }
  image_pull_secret {
    name = "gcp-registry"
  }
}