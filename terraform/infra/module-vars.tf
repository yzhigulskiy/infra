variable "gcp_registry_username" {}
// Logging and Trace settings
variable "sink_project_number" {}
variable "sink_project_id" {}
variable "logging_sink_filter" {}
variable "logging_sink_bucket_destination" {}
variable "trace_sink_dataset" {}