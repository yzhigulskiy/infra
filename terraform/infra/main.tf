data "google_secret_manager_secret_version" "gcp-registry" {
  secret  = "gcp-registry"
  version = "1"
}

data "terraform_remote_state" "kubernetes" {
  backend = "gcs"
  config = {
    bucket = "tf-state-${var.PROJECT}"
    prefix = "kubernetes/state"
  }
}

data "google_secret_manager_secret_version" "gke_admin_password" {
  secret  = "gke_admin_password"
  version = "1"
}

provider "helm" {
  kubernetes {
    host                   = "https://${data.terraform_remote_state.kubernetes.outputs.host}"
    username               = data.terraform_remote_state.kubernetes.outputs.username
    password               = data.google_secret_manager_secret_version.gke_admin_password.secret_data
    cluster_ca_certificate = data.terraform_remote_state.kubernetes.outputs.cluster_ca_certificate
    load_config_file       = false
  }
}

module "infra" {
  source                = "../modules/infra"
  GCP_REGISTRY_USER     = data.google_secret_manager_secret_version.gcp-registry.secret_data
  gcp_registry_username = var.gcp_registry_username
}

module "nats" {
  source                = "../modules/nats-io"
  GCP_REGISTRY_USER     = data.google_secret_manager_secret_version.gcp-registry.secret_data
  gcp_registry_username = var.gcp_registry_username
}

module "etcd" {
  source                = "../modules/etcd"
  GCP_REGISTRY_USER     = data.google_secret_manager_secret_version.gcp-registry.secret_data
  gcp_registry_username = var.gcp_registry_username
}

module "sink" {
  PROJECT                         = var.PROJECT
  source                          = "../modules/sink"
  sink_project_number             = var.sink_project_number
  sink_project_id                 = var.sink_project_id
  logging_sink_filter             = var.logging_sink_filter
  logging_sink_bucket_destination = var.logging_sink_bucket_destination
  trace_sink_dataset              = var.trace_sink_dataset
}