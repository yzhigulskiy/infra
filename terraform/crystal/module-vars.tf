variable "tenant_minio_endpoint" {
  description = ""
}
variable "dataset_template_bucket" {}
variable "tenant_base_endpoint" {}
variable "network_id" {
  description = "Network ID"
}
variable "master_authorized_networks" {
  description = "List of master authorized networks. If none are provided, disallow external access (except the cluster node IPs, which GKE automatically whitelists)."
  default     = []
}
variable "users" {
  description = "User for SQL Instance"
}
variable "cloudsql_tier" {}
variable "tenant_name" {
  description = "Tenant name "
}
variable "retrain_nats_endpoint" {
  description = "Nats endpoint"
  type = string
}
variable "cloudsql_deletion_protection" {
  description = "Protect SQL Instance from termination"
}
variable "service_versions" {
  type = map
  default = {
  }
}

data "terraform_remote_state" "kubernetes" {
  backend = "gcs"
  config = {
    bucket = "tf-state-${var.PROJECT}"
    prefix = "kubernetes/state"
  }
}

data "google_secret_manager_secret_version" "gke_admin_password" {
  secret  = "gke_admin_password"
  version = "1"
}

provider "helm" {
  kubernetes {
    host                   = "https://${data.terraform_remote_state.kubernetes.outputs.host}"
    username               = data.terraform_remote_state.kubernetes.outputs.username
    password               = data.google_secret_manager_secret_version.gke_admin_password.secret_data
    cluster_ca_certificate = data.terraform_remote_state.kubernetes.outputs.cluster_ca_certificate
    load_config_file       = false
  }
}
