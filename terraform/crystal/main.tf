module "tenant" {
  source                     = "../../module/"
  tenant_name                = var.tenant_name
  REGION                     = var.REGION
  PROJECT                    = var.PROJECT
  cloudsql_tier              = var.cloudsql_tier
  users                      = var.users
  master_authorized_networks = var.master_authorized_networks
  network_id                 = var.network_id
  tenant_base_endpoint       = var.tenant_base_endpoint
  dataset_template_bucket    = var.dataset_template_bucket
  tenant_minio_endpoint      = var.tenant_minio_endpoint
  retrain_nats_endpoint      = var.retrain_nats_endpoint
  service_versions           = var.service_versions
  deletion_protection        = var.cloudsql_deletion_protection
}