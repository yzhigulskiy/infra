data "template_file" "crystal-alert-classifier-serving-it-values" {
  template = file("${path.module}/../../templates/crystal-alert-classifier-serving-it-develop.yaml")
  vars = {
    crystal_alert_classifier_serving_it_serve      = var.crystal_alert_classifier_serving_it_serve
    crystal_alert_classifier_serving_it_controller = var.crystal_alert_classifier_serving_it_controller
  }
}

resource "helm_release" "crystal-alert-classifier-serving-it" {
  depends_on = [
    data.template_file.crystal-alert-classifier-serving-it-values
  ]
  timeout    = 600
  name       = "alert-classifier-it"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-alert-classifier-serving-it-values.rendered
  ]
}