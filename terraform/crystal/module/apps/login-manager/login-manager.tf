data "google_secret_manager_secret_version" "login-manager" {
  secret  = "${var.tenant_name}-login-manager"
  version = "1"
}

data "google_secret_manager_secret_version" "tenant-owner" {
  secret  = "${var.tenant_name}-tenant-owner"
  version = "1"
}

resource "kubernetes_secret" "tenant-owner" {
  metadata {
    name      = "${var.tenant_name}-2-stage-tenant-owner"
    namespace = "infrustructure-provision"
  }
  data = {
    username = "${var.tenant_name}-2-stage-tenant-owner"
    password = data.google_secret_manager_secret_version.tenant-owner.secret_data
  }

  type = "kubernetes.io/basic-auth"
}

data "template_file" "crystal-login-manager-values" {
  depends_on = [
    data.google_secret_manager_secret_version.login-manager
  ]
  template = file("${path.module}/../../templates/crystal-login-manager-develop.yaml")
  vars = {
    cloud_sql_databases_name        = var.cloud_sql_database_name
    cloud_sql_database_ip           = var.cloud_sql_database_ip
    login_manager_password          = data.google_secret_manager_secret_version.login-manager.secret_data
    tenant_base_endpoint            = var.tenant_base_endpoint
    tenant_name                     = var.tenant_name
    crystal_login_manager           = var.crystal_login_manager
    crystal_login_manager_migration = var.crystal_login_manager_migration
  }
}

resource "helm_release" "crystal-login-manager" {
  depends_on = [
    data.template_file.crystal-login-manager-values
  ]
  timeout    = 600
  name       = "login-manager"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-login-manager-values.rendered
  ]
}

resource "helm_release" "crystal-sql-provision" {
  depends_on = [
    helm_release.crystal-login-manager
  ]
  timeout    = 600
  name       = "${var.tenant_name}-sql-provision-2-stage"
  chart      = "crystal-sql-provision"
  repository = var.helm_repository_url
  namespace  = "infrustructure-provision"
  values = [<<EOF
  workspace:
    name: "${var.tenant_name}-2-stage"
  sql:
    ip: ${var.cloud_sql_database_ip}
    database: ${var.tenant_name}
EOF
  ]
}