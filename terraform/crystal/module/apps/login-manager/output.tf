output "login-manager" {
  value = helm_release.crystal-login-manager
}
output "sql_provision" {
  value = helm_release.crystal-sql-provision
}