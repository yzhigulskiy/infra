variable "helm_repository_url" {}
variable "helm_release_name" {}
variable "crystal_intent_classifier_serving_en_server" {}
variable "crystal_intent_classifier_serving_en_controller" {}
variable "tenant_name" {}
