data "template_file" "crystal-intent-classifier-serving-en-values" {
  template = file("${path.module}/../../templates/crystal-intent-classifier-serving-en-develop.yaml")
  vars = {
    crystal_intent_classifier_serving_en_server     = var.crystal_intent_classifier_serving_en_server
    crystal_intent_classifier_serving_en_controller = var.crystal_intent_classifier_serving_en_controller
  }
}

resource "helm_release" "crystal-intent-classifier-serving-en" {
  depends_on = [
    data.template_file.crystal-intent-classifier-serving-en-values
  ]
  timeout    = 600
  name       = "intent-classifier-en"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-intent-classifier-serving-en-values.rendered
  ]
}