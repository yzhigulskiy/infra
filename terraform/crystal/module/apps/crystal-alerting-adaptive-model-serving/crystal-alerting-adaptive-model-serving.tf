data "template_file" "crystal-alerting-adaptive-model-serving" {
  template = file("${path.module}/../../templates/crystal-alerting-adaptive-model-serving-values-develop.yaml")
  vars = {
    crystal_alerting_adaptive_model_serving = var.crystal_alerting_adaptive_model_serving
  }
}

resource "helm_release" "crystal-alerting-adaptive-model-serving" {
  depends_on = [
    data.template_file.crystal-alerting-adaptive-model-serving
  ]
  timeout    = 600
  name       = "alerting-adaptive-model-serving"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-alerting-adaptive-model-serving.rendered
  ]
}