data "template_file" "crystal-suggestion-tips-manager-values" {
  template = file("${path.module}/../../templates/crystal-suggestion-tips-manager-values-develop.yaml")
  vars = {
    crystal_suggestion_tips_manager           = var.crystal_suggestion_tips_manager
    crystal_suggestion_tips_manager_migration = var.crystal_suggestion_tips_manager_migration
  }
}

resource "helm_release" "suggestion_tips_manager" {
  depends_on = [
    data.template_file.crystal-suggestion-tips-manager-values
  ]
  timeout    = 600
  name       = "crystal-suggestion-tips-manager"
  chart      = "crystal-suggestion-tips-manager"
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-suggestion-tips-manager-values.rendered
  ]
}