data "template_file" "crystal-datasource-adapter-postgresql-values" {
  template = file("${path.module}/../../templates/crystal-datasource-adapter-postgresql-develop.yaml")
  vars = {
    crystal_datasource_adapter_postgresql = var.crystal_datasource_adapter_postgresql
  }
}

resource "helm_release" "crystal-datasource-adapter-postgresql" {
  depends_on = [
    data.template_file.crystal-datasource-adapter-postgresql-values
  ]
  timeout    = 600
  name       = "pg-adapter"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-datasource-adapter-postgresql-values.rendered
  ]
}