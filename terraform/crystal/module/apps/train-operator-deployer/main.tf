data "template_file" "crystal-train-operator-deployer-values" {
  template = file("${path.module}/../../templates/crystal-train-operator-deployer-values.yaml")
  vars = {
    namespace                       = var.tenant_name
    crystal_train_operator_deployer = var.crystal_train_operator_deployer
  }
}

resource "helm_release" "crystal-train-operator-deployer" {
  depends_on = [
    data.template_file.crystal-train-operator-deployer-values
  ]
  timeout    = 60
  name       = "train-operator-deployer"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-train-operator-deployer-values.rendered
  ]
}
