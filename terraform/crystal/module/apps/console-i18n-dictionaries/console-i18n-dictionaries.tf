data "template_file" "console-i18n-values" {
  template = file("${path.module}/../../templates/crystal-console-i18n-dictionaries.yaml")
  vars = {
    crystal_console_i18n_dictionaries = var.crystal_console_i18n_dictionaries
  }
}

resource "helm_release" "console-i18n-dictionaries" {
  timeout    = 600
  name       = "console-i18n-dictionaries"
  chart      = "console-i18n-dictionaries"
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.console-i18n-values.rendered
  ]
}