data "template_file" "hydra-values" {
  template = file("${path.module}/../../templates/hydra-develop.yaml")
  vars = {
    cloud_sql_databases_name = var.cloud_sql_database_name
    cloud_sql_database_ip    = var.cloud_sql_database_ip
    hydra_password           = data.google_secret_manager_secret_version.hydra.secret_data
    tenant_base_endpoint     = var.tenant_base_endpoint
    hydra                    = var.hydra
  }
}

data "google_secret_manager_secret_version" "hydra" {
  secret  = "${var.tenant_name}-hydraprov"
  version = "1"
}

resource "helm_release" "hydra" {
  depends_on = [
    data.template_file.hydra-values
  ]
  timeout    = 600
  name       = var.helm_release_name
  chart      = "hydra"
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.hydra-values.rendered
  ]
}