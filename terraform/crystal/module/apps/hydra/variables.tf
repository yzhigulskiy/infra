variable "helm_repository_url" {}
variable "helm_release_name" {}
variable "cloud_sql_database_ip" {}
variable "hydra" {}
variable "cloud_sql_database_name" {}
variable "tenant_name" {}
variable "tenant_base_endpoint" {}
