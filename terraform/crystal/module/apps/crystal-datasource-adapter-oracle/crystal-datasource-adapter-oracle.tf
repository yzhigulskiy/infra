data "template_file" "crystal-datasource-adapter-oracle-values" {
  template = file("${path.module}/../../templates/crystal-datasource-adapter-oracle-develop.yaml")
  vars = {
    crystal_datasource_adapter_oracle = var.crystal_datasource_adapter_oracle
  }
}

resource "helm_release" "crystal-datasource-adapter-oracle" {
  depends_on = [
    data.template_file.crystal-datasource-adapter-oracle-values
  ]
  timeout    = 600
  name       = "oracle-adapter"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-datasource-adapter-oracle-values.rendered
  ]
}