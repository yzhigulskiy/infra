variable "helm_repository_url" {}
variable "helm_release_name" {}
variable "cloud_sql_database_ip" {}
variable "crystal_aggregation_classifier_it" {}
variable "cloud_sql_database_name" {}
variable "tenant_name" {}
