data "template_file" "crystal-aggregation-classifier-it-values" {
  template = file("${path.module}/../../templates/crystal-aggregation-classifier-it-values.yaml")
  vars = {
    crystal_aggregation_classifier_it = var.crystal_aggregation_classifier_it
  }
}

resource "helm_release" "crystal-aggregation-classifier-it" {
  depends_on = [
    data.template_file.crystal-aggregation-classifier-it-values
  ]
  timeout    = 600
  name       = "agg-cls-it"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-aggregation-classifier-it-values.rendered
  ]
}