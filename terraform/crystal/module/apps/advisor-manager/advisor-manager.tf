data "google_secret_manager_secret_version" "advisor-manager" {
  secret  = "${var.tenant_name}-advisor-manager"
  version = "1"
}

data "template_file" "crystal-advisor-manager-values" {
  template = file("${path.module}/../../templates/crystal-advisor-manager-values-develop.yaml")
  vars = {
    cloud_sql_databases_name = var.cloud_sql_database_name
    cloud_sql_database_ip    = var.cloud_sql_database_ip
    advisor_manager_password = data.google_secret_manager_secret_version.advisor-manager.secret_data
    dataset_template_bucket  = var.dataset_template_bucket
    retrain_nats_endpoint    = var.retrain_nats_endpoint
    crystal_advisor_manager  = var.crystal_advisor_manager
    tenant_name              = var.tenant_name
  }
}

resource "helm_release" "crystal-advisor-manager" {
  depends_on = [
    data.template_file.crystal-advisor-manager-values
  ]
  timeout    = 600
  name       = "advisor-manager"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-advisor-manager-values.rendered
  ]
}

