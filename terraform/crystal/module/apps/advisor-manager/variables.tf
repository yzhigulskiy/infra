variable "helm_repository_url" {}
variable "helm_release_name" {}
variable "cloud_sql_database_ip" {}
variable "cloud_sql_database_name" {}
variable "crystal_advisor_manager" {}
variable "dataset_template_bucket" {}
variable "tenant_name" {}
variable "retrain_nats_endpoint" {}
