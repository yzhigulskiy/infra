data "template_file" "crystal-topic-builder-values" {
  template = file("${path.module}/../../templates/crystal-topic-builder-values-develop.yaml")
  vars = {
    crystal_topic_builder = var.crystal_topic_builder
  }
}

resource "helm_release" "crystal-topic-builder" {
  depends_on = [
    data.template_file.crystal-topic-builder-values
  ]
  timeout    = 600
  name       = "topic-builder"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-topic-builder-values.rendered
  ]
}