data "google_secret_manager_secret_version" "nats" {
  secret  = "${var.tenant_name}-nats"
  version = "1"
}

data "template_file" "crystal-nats-values" {
  template = file("${path.module}/../../templates/crystal-nats-develop.yaml")
  vars = {
    cloud_sql_databases_name = var.cloud_sql_database_name
    cloud_sql_database_ip    = var.cloud_sql_database_ip
    nats_password            = data.google_secret_manager_secret_version.nats.secret_data
    tenant_name              = var.tenant_name
    crystal_nats_migration   = var.crystal_nats_migration
  }
}

resource "helm_release" "nats" {
  depends_on = [
    data.template_file.crystal-nats-values
  ]
  timeout   = 600
  name      = "${var.tenant_name}-nats"
  chart     = "${path.module}/../../helm_charts/nats-cluster"
  namespace = var.tenant_name
  values = [
    data.template_file.crystal-nats-values.rendered
  ]
}