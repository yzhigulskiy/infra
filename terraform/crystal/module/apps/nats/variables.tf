variable "helm_repository_url" {}
variable "crystal_nats_migration" {}
variable "cloud_sql_database_ip" {}
variable "cloud_sql_database_name" {}
variable "GCP_REGISTRY_USER" {}
variable "gcp_registry_username" {}
variable "tenant_name" {}