data "google_secret_manager_secret_version" "alerting-squad" {
  secret  = "${var.tenant_name}-alerting-squad"
  version = "1"
}

data "template_file" "crystal-alerting-squad-values" {
  template = file("${path.module}/../../templates/crystal-alerting-values-squad-develop.yaml")
  vars = {
    cloud_sql_databases_name         = var.cloud_sql_database_name
    cloud_sql_database_ip            = var.cloud_sql_database_ip
    alerting-squad_password          = data.google_secret_manager_secret_version.alerting-squad.secret_data
    crystal_alerting_squad           = var.crystal_alerting_squad
    crystal_alerting_squad_migration = var.crystal_alerting_squad_migration
    tenant_name                      = var.tenant_name
  }
}

resource "helm_release" "crystal-alerting-squad" {
  depends_on = [
    data.template_file.crystal-alerting-squad-values
  ]
  timeout    = 600
  name       = var.helm_release_name
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-alerting-squad-values.rendered
  ]
}