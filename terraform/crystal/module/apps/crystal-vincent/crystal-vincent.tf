data "template_file" "crystal-vincent-values" {
  template = file("${path.module}/../../templates/crystal-vincent-develop.yaml")
  vars = {
    dataset_template_bucket = var.dataset_template_bucket
    crystal_vincent         = var.crystal_vincent
  }
}

resource "helm_release" "crystal-vincent" {
  depends_on = [
    data.template_file.crystal-vincent-values
  ]
  timeout    = 600
  name       = "vincent"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-vincent-values.rendered
  ]
}