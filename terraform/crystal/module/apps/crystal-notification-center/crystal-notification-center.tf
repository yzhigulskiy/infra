data "template_file" "crystal-notification-center-values" {
  template = file("${path.module}/../../templates/crystal-notification-center-squad-develop.yaml")
  vars = {
    notification-center_password                = data.google_secret_manager_secret_version.notification-center.secret_data
    cloud_sql_databases_name                    = var.cloud_sql_database_name
    cloud_sql_database_ip                       = var.cloud_sql_database_ip
    crystal_notification_center_squad           = var.crystal_notification_center_squad
    crystal_notification_center_squad_migration = var.crystal_notification_center_squad_migration
    tenant_name                                 = var.tenant_name
  }
}

data "google_secret_manager_secret_version" "notification-center" {
  secret  = "${var.tenant_name}-notification-center"
  version = "1"
}

resource "helm_release" "crystal-notification-center" {
  depends_on = [
    data.template_file.crystal-notification-center-values
  ]
  timeout    = 600
  name       = "notification-center"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-notification-center-values.rendered
  ]
}