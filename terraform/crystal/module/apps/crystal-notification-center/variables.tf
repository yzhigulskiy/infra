variable "helm_repository_url" {}
variable "helm_release_name" {}
variable "crystal_notification_center_squad" {}
variable "crystal_notification_center_squad_migration" {}
variable "tenant_name" {}
variable "cloud_sql_database_ip" {}
variable "cloud_sql_database_name" {}