data "template_file" "crystal-general-classifier-en-values" {
  template = file("${path.module}/../../templates/crystal-general-classifier-en-develop.yaml")
  vars = {
    crystal_general_classifier_en = var.crystal_general_classifier_en
  }
}

resource "helm_release" "crystal-general-classifier-en" {
  depends_on = [
    data.template_file.crystal-general-classifier-en-values
  ]
  timeout    = 600
  name       = "gen-cls-en"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-general-classifier-en-values.rendered
  ]
}