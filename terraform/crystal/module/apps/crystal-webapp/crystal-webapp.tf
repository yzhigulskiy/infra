data "template_file" "crystal-webapp-values" {
  template = file("${path.module}/../../templates/crystal-webapp-develop.yaml")
  vars = {
    tenant_name          = var.tenant_name
    tenant_base_endpoint = var.tenant_base_endpoint
    crystal_webapp       = var.crystal_webapp
  }
}

resource "helm_release" "crystal-webapp" {
  depends_on = [
    data.template_file.crystal-webapp-values,
  ]
  timeout    = 600
  name       = "webapp"
  chart      = "crystal-webapp"
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-webapp-values.rendered
  ]
}