data "google_secret_manager_secret_version" "nlp-entities-manager" {
  secret  = "${var.tenant_name}-nlp-entities-manager"
  version = "1"
}

data "template_file" "crystal-nlp-entities-manager-values" {
  template = file("${path.module}/../../templates/crystal-nlp-entities-manager-develop.yaml")
  vars = {
    cloud_sql_databases_name               = var.cloud_sql_database_name
    cloud_sql_database_ip                  = var.cloud_sql_database_ip
    nlp-entities-manager_password          = data.google_secret_manager_secret_version.nlp-entities-manager.secret_data
    namespace                              = var.tenant_name
    crystal_nlp_entities_manager           = var.crystal_nlp_entities_manager
    crystal_nlp_entities_manager_migration = var.crystal_nlp_entities_manager_migration
    tenant_name                            = var.tenant_name
  }
}

resource "helm_release" "nlp-entities-manager" {
  depends_on = [
    data.template_file.crystal-nlp-entities-manager-values
  ]
  timeout    = 600
  name       = "nlp-entities-manager"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-nlp-entities-manager-values.rendered
  ]
}

