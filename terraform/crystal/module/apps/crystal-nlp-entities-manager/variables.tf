variable "helm_repository_url" {}
variable "helm_release_name" {}
variable "cloud_sql_database_ip" {}
variable "crystal_nlp_entities_manager" {}
variable "cloud_sql_database_name" {}
variable "crystal_nlp_entities_manager_migration" {}
variable "tenant_name" {}
