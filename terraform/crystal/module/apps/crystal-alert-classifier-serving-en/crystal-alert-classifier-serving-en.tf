data "template_file" "crystal-alert-classifier-serving-en-values" {
  template = file("${path.module}/../../templates/crystal-alert-classifier-serving-en-develop.yaml")
  vars = {
    crystal_alert_classifier_serving_en_serve      = var.crystal_alert_classifier_serving_en_serve
    crystal_alert_classifier_serving_en_controller = var.crystal_alert_classifier_serving_en_controller
  }
}

resource "helm_release" "crystal-alert-classifier-serving-en" {
  depends_on = [
    data.template_file.crystal-alert-classifier-serving-en-values
  ]
  timeout    = 600
  name       = "alert-classifier-en"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-alert-classifier-serving-en-values.rendered
  ]
}