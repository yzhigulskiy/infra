data "template_file" "crystal-train-operator-values" {
  template = file("${path.module}/../../templates/crystal-train-operator-values.yaml")
  vars = {
    dataset_template_bucket = var.dataset_template_bucket
    crystal_train_operator = var.crystal_train_operator
  }
}

resource "helm_release" "crystal-train-operator" {
  depends_on = [
    data.template_file.crystal-train-operator-values
  ]
  timeout    = 60
  name       = "train-operator"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-train-operator-values.rendered
  ]
}
