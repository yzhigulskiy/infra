variable "helm_repository_url" {}
variable "helm_release_name" {}
variable "cloud_sql_database_ip" {}
variable "crystal_settings_service" {}
variable "cloud_sql_database_name" {}
variable "crystal_settings_service_migration" {}
variable "tenant_base_endpoint" {}
variable "tenant_name" {}
