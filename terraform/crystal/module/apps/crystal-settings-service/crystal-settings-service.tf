data "google_secret_manager_secret_version" "system-settings" {
  secret  = "${var.tenant_name}-system-settings"
  version = "1"
}

data "template_file" "crystal-settings-service-values" {
  template = file("${path.module}/../../templates/crystal-settings-service-values-develop.yaml")
  vars = {
    cloud_sql_databases_name           = var.cloud_sql_database_name
    cloud_sql_database_ip              = var.cloud_sql_database_ip
    system-settings_password           = data.google_secret_manager_secret_version.system-settings.secret_data
    tenant_base_endpoint               = var.tenant_base_endpoint
    tenant_name                        = var.tenant_name
    crystal_settings_service           = var.crystal_settings_service
    crystal_settings_service_migration = var.crystal_settings_service_migration
  }
}

resource "helm_release" "crystal-settings-service" {
  depends_on = [
    data.template_file.crystal-settings-service-values
  ]
  timeout    = 600
  name       = "settings-service"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-settings-service-values.rendered
  ]
}

