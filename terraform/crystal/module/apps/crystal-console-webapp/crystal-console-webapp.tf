data "template_file" "crystal-console-webapps-values" {
  template = file("${path.module}/../../templates/crystal-console-webapp-develop.yaml")
  vars = {
    tenant_base_endpoint   = var.tenant_base_endpoint
    crystal_console_webapp = var.crystal_console_webapp
  }
}

resource "helm_release" "crystal-console-webapps" {
  depends_on = [
    data.template_file.crystal-console-webapps-values,
  ]
  timeout    = 600
  name       = "console-webapp"
  chart      = "crystal-console-webapp"
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-console-webapps-values.rendered
  ]
}