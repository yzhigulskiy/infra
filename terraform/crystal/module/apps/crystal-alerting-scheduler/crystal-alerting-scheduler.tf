data "google_secret_manager_secret_version" "alerting-scheduler" {
  secret  = "${var.tenant_name}-alerting-scheduler"
  version = "1"
}

data "template_file" "crystal-alerting-scheduler-values" {
  template = file("${path.module}/../../templates/crystal-alerting-scheduler-values-develop.yaml")
  vars = {
    cloud_sql_databases_name             = var.cloud_sql_database_name
    cloud_sql_database_ip                = var.cloud_sql_database_ip
    alerting-scheduler_password          = data.google_secret_manager_secret_version.alerting-scheduler.secret_data
    crystal_alerting_scheduler           = var.crystal_alerting_scheduler
    crystal_alerting_scheduler_migration = var.crystal_alerting_scheduler_migration
  }
}

resource "helm_release" "crystal-alerting-scheduler" {
  depends_on = [
    data.template_file.crystal-alerting-scheduler-values
  ]
  timeout    = 600
  name       = "alerting-scheduler"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-alerting-scheduler-values.rendered
  ]
}