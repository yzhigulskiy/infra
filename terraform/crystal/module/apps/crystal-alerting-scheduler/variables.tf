variable "helm_repository_url" {}
variable "helm_release_name" {}
variable "crystal_alerting_scheduler" {}
variable "crystal_alerting_scheduler_migration" {}
variable "cloud_sql_database_name" {}
variable "cloud_sql_database_ip" {}
variable "tenant_name" {}