data "template_file" "crystal-etcd-values" {
  template = file("${path.module}/../../templates/crystal-etcd-cluster-develop.yaml")
  vars = {
    crystal_etcd_cluster = var.crystal_etcd_cluster
  }
}

resource "helm_release" "etcd" {
  timeout    = 600
  name       = "etcd"
  chart      = "etcd-cluster"
  repository = "${path.module}/../../helm_charts"
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-etcd-values.rendered
  ]
}