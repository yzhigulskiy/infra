data "template_file" "crystal-datasource-adapter-mssql-values" {
  template = file("${path.module}/../../templates/crystal-datasource-adapter-mssql-develop.yaml")
  vars = {
    crystal_datasource_adapter_mssql = var.crystal_datasource_adapter_mssql
  }
}

resource "helm_release" "crystal-datasource-adapter-mssql" {
  depends_on = [
    data.template_file.crystal-datasource-adapter-mssql-values
  ]
  timeout    = 600
  name       = "mssql-adapter"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-datasource-adapter-mssql-values.rendered
  ]
}