data "template_file" "crystal-permissions-service-values" {
  template = file("${path.module}/../../templates/crystal-permissions-service-develop.yaml")
  vars = {
    crystal_permissions_service = var.crystal_permissions_service
  }
}

resource "helm_release" "crystal-permissions-service" {
  depends_on = [
    data.template_file.crystal-permissions-service-values
  ]
  timeout    = 600
  name       = "permission"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-permissions-service-values.rendered
  ]
}