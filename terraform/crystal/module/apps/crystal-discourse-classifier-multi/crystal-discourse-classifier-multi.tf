data "template_file" "crystal-discourse-classifier-multi-values" {
  template = file("${path.module}/../../templates/crystal-discourse-classifier-multi-develop.yaml")
  vars = {
    crystal_discourse_classifier_multi = var.crystal_discourse_classifier_multi
  }
}

resource "helm_release" "crystal-discourse-classifier-multi" {
  depends_on = [
    data.template_file.crystal-discourse-classifier-multi-values
  ]
  timeout    = 600
  name       = "disc-cls"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-discourse-classifier-multi-values.rendered
  ]
}