data "template_file" "crystal-pablo-values" {
  template = file("${path.module}/../../templates/pablo-values-develop.yaml")
  vars = {
    dataset_template_bucket = var.dataset_template_bucket
    pablo                   = var.pablo
  }
}

resource "helm_release" "crystal-pablo" {
  depends_on = [
    data.template_file.crystal-pablo-values
  ]
  timeout    = 600
  name       = "pablo"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-pablo-values.rendered
  ]
}