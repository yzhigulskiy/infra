variable "helm_repository_url" {}
variable "helm_release_name" {}
variable "cloud_sql_database_ip" {}
variable "crystal_idp_adapter_local" {}
variable "crystal_idp_adapter_local_migration" {}
variable "cloud_sql_database_name" {}
variable "tenant_name" {}