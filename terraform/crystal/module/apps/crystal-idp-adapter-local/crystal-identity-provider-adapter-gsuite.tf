data "google_secret_manager_secret_version" "identity-provider-adapter-local" {
  secret  = "${var.tenant_name}-identity-provider-adapter-local"
  version = "1"
}

data "template_file" "crystal-idp-adapter-local-values" {
  template = file("${path.module}/../../templates/crystal-idp-adapter-local-develop.yaml")
  vars = {
    identity-provider-adapter-local_password = data.google_secret_manager_secret_version.identity-provider-adapter-local.secret_data
    cloud_sql_database_ip                    = var.cloud_sql_database_ip
    cloud_sql_databases_name                 = var.cloud_sql_database_name
    tenant_name                              = var.tenant_name
    crystal_idp_adapter_local                = var.crystal_idp_adapter_local
    crystal_idp_adapter_local_migration      = var.crystal_idp_adapter_local_migration
  }
}

resource "helm_release" "crystal-idp-adapter-local" {
  depends_on = [
    data.template_file.crystal-idp-adapter-local-values
  ]
  timeout    = 600
  name       = "idp-local"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-idp-adapter-local-values.rendered
  ]
}