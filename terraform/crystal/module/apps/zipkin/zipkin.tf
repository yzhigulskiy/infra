resource "helm_release" "zipkin" {
  timeout    = 600
  name       = "zipkin"
  chart      = "zipkin"
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
  ]
}