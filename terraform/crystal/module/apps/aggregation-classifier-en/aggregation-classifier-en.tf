data "template_file" "crystal-aggregation-classifier-values" {
  template = file("${path.module}/../../templates/crystal-aggregation-classifier-en-values.yaml")
  vars = {
    crystal_aggregation_classifier_en = var.crystal_aggregation_classifier_en
  }
}

resource "helm_release" "crystal-aggregation-classifier" {
  depends_on = [
    data.template_file.crystal-aggregation-classifier-values
  ]
  timeout    = 600
  name       = "agg-cls-en"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-aggregation-classifier-values.rendered
  ]
}