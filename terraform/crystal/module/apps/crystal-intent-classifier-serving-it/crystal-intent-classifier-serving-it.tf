data "template_file" "crystal-intent-classifier-serving-it-values" {
  template = file("${path.module}/../../templates/crystal-intent-classifier-serving-it-develop.yaml")
  vars = {
    workspace                                       = var.tenant_name
    crystal_intent_classifier_serving_it_serve      = var.crystal_intent_classifier_serving_it_serve
    crystal_intent_classifier_serving_it_controller = var.crystal_intent_classifier_serving_it_controller
  }
}

resource "helm_release" "crystal-intent-classifier-serving-it" {
  depends_on = [
    data.template_file.crystal-intent-classifier-serving-it-values
  ]
  timeout    = 600
  name       = "intent-classifier-it"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-intent-classifier-serving-it-values.rendered
  ]
}