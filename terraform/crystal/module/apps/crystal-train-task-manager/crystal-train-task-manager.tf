data "google_secret_manager_secret_version" "train-task-manager" {
  secret  = "${var.tenant_name}-train-task-manager"
  version = "1"
}

data "template_file" "crystal-train-task-manager-values" {
  template = file("${path.module}/../../templates/crystal-train-task-manager-values.yaml")
  vars = {
    tenant_name                 = var.tenant_name
    crystal_train_task_manager  = var.crystal_train_task_manager
    train-task-manager-password = data.google_secret_manager_secret_version.train-task-manager.secret_data
    cloud_sql_database_name     = var.cloud_sql_database_name
    cloud_sql_database_ip       = var.cloud_sql_database_ip
  }
}

resource "helm_release" "crystal-topic-widget" {
  depends_on = [
    data.template_file.crystal-train-task-manager-values
  ]
  timeout    = 600
  name       = "crystal-train-task-manager"
  chart      = "crystal-train-task-manager"
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-train-task-manager-values.rendered
  ]
}