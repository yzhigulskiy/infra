data "template_file" "crystal-identity-provider-adapter-gsuite-values" {
  template = file("${path.module}/../../templates/crystal-identity-provider-adapter-gsuite-develop.yaml")
  vars = {
    tenant_name                              = var.tenant_name
    crystal_identity_provider_adapter_gsuite = var.crystal_identity_provider_adapter_gsuite
  }
}

resource "helm_release" "crystal-identity-provider-adapter-gsuite" {
  depends_on = [
    data.template_file.crystal-identity-provider-adapter-gsuite-values
  ]
  timeout    = 600
  name       = "idp-gsuite"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-identity-provider-adapter-gsuite-values.rendered
  ]
}