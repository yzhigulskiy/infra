data "google_secret_manager_secret_version" "louvre" {
  secret  = "${var.tenant_name}-louvre"
  version = "1"
}

data "template_file" "crystal-louvre-values" {
  template = file("${path.module}/../../templates/crystal-louvre-develop.yaml")
  vars = {
    cloud_sql_databases_name = var.cloud_sql_database_name
    cloud_sql_database_ip    = var.cloud_sql_database_ip
    louvre_password          = data.google_secret_manager_secret_version.louvre.secret_data
    namespace                = var.tenant_name
    dataset_template_bucket  = var.dataset_template_bucket
    crystal_louvre           = var.crystal_louvre
    crystal_louvre_migration = var.crystal_louvre_migration
  }
}

resource "helm_release" "crystal-louvre" {
  depends_on = [
    data.template_file.crystal-louvre-values
  ]
  timeout    = 600
  name       = "louvre"
  version    = "v1.0.1"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-louvre-values.rendered
  ]
}
