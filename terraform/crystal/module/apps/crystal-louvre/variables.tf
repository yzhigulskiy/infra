variable "helm_repository_url" {}
variable "helm_release_name" {}
variable "cloud_sql_database_ip" {}
variable "crystal_louvre" {}
variable "cloud_sql_database_name" {}
variable "crystal_louvre_migration" {}
variable "dataset_template_bucket" {}
variable "tenant_name" {}
