data "template_file" "keto-values" {
  template = file("${path.module}/../../templates/keto-develop.yaml")
  vars = {
    cloud_sql_databases_name = var.cloud_sql_database_name
    cloud_sql_database_ip    = var.cloud_sql_database_ip
    keto_password            = data.google_secret_manager_secret_version.keto.secret_data
    keto                     = var.keto
  }
}

data "google_secret_manager_secret_version" "keto" {
  secret  = "${var.tenant_name}-ketoprov"
  version = "1"
}

resource "helm_release" "keto" {
  depends_on = [
    data.template_file.keto-values
  ]
  timeout    = 600
  name       = var.helm_release_name
  chart      = "keto"
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.keto-values.rendered
  ]
}