variable "helm_repository_url" {}
variable "helm_release_name" {}
variable "cloud_sql_database_ip" {}
variable "crystal_talent_widget" {}
variable "cloud_sql_database_name" {}
variable "crystal_talent_widget_migration" {}
variable "tenant_name" {}
