data "google_secret_manager_secret_version" "topic-service" {
  secret  = "${var.tenant_name}-topic-service"
  version = "1"
}

data "template_file" "crystal-talent-widget-values" {
  template = file("${path.module}/../../templates/crystal-talent-widget-delevop.yaml")
  vars = {
    cloud_sql_databases_name        = var.cloud_sql_database_name
    cloud_sql_database_ip           = var.cloud_sql_database_ip
    topic-service_password          = data.google_secret_manager_secret_version.topic-service.secret_data
    namespace                       = var.tenant_name
    crystal_talent_widget           = var.crystal_talent_widget
    crystal_talent_widget_migration = var.crystal_talent_widget_migration
  }
}

resource "helm_release" "crystal-talent-widget" {
  depends_on = [
    data.template_file.crystal-talent-widget-values
  ]
  timeout    = 600
  name       = "twidget"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-talent-widget-values.rendered
  ]
}

