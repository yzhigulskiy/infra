data "template_file" "crystal-speech-to-text-values" {
  template = file("${path.module}/../../templates/crystal-speech-service-values-develop.yaml")
  vars = {
    tenant_name            = var.tenant_name
    tenant_base_endpoint   = var.tenant_base_endpoint
    crystal_speech_to_text = var.crystal_speech_to_text
  }
}

resource "helm_release" "crystal-speech-to-text" {
  depends_on = [
    data.template_file.crystal-speech-to-text-values,
  ]
  timeout    = 600
  name       = "speech-service"
  chart      = "speech_to_text_gateway"
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-speech-to-text-values.rendered
  ]
}