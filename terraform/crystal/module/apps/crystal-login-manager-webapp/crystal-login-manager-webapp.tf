data "template_file" "crystal-login-manager-webapp-values" {
  template = file("${path.module}/../../templates/crystal-login-manager-webapp-develop.yaml")
  vars = {
    crystal_login_manager_webapp = var.crystal_login_manager_webapp
  }
}

resource "helm_release" "crystal-login-manager-webapp-new" {
  depends_on = [
    data.template_file.crystal-login-manager-webapp-values
  ]
  timeout    = 600
  name       = "login-manager-webapp"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-login-manager-webapp-values.rendered
  ]
}