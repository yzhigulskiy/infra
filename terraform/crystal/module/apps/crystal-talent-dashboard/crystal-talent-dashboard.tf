data "template_file" "crystal-talent-dashboard-values" {
  template = file("${path.module}/../../templates/crystal-talent-dashboard-service-values-develop.yaml")
  vars = {
    tenant_name              = var.tenant_name
    crystal_talent_dashboard = var.crystal_talent_dashboard
  }
}

resource "helm_release" "crystal-talent-dashboard" {
  depends_on = [
    data.template_file.crystal-talent-dashboard-values,
  ]
  timeout    = 600
  name       = "talent-dashboard"
  chart      = "crystal-talent-dashboard-service"
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-talent-dashboard-values.rendered
  ]
}