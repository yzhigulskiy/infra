data "template_file" "crystal-general-classifier-it-values" {
  template = file("${path.module}/../../templates/crystal-general-classifier-it-develop.yaml")
  vars = {
    crystal_general_classifier_it = var.crystal_general_classifier_it
  }
}

resource "helm_release" "crystal-general-classifier-it" {
  depends_on = [
    data.template_file.crystal-general-classifier-it-values
  ]
  timeout    = 600
  name       = "gen-cls-it"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-general-classifier-it-values.rendered
  ]
}