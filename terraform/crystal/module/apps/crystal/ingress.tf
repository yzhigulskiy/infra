data "terraform_remote_state" "kubernetes" {
  backend = "gcs"
  config = {
    bucket = "tf-state-${var.PROJECT}"
    prefix = "kubernetes/state"
  }
}

data "google_secret_manager_secret_version" "gke_admin_password" {
  secret  = "gke_admin_password"
  version = "1"
}

resource "null_resource" "crystal-io" {
  provisioner "local-exec" {
    command = <<EOT
kubectl apply --server=https://${data.terraform_remote_state.kubernetes.outputs.host} --username=${data.terraform_remote_state.kubernetes.outputs.username} --password=${data.google_secret_manager_secret_version.gke_admin_password.secret_data} --insecure-skip-tls-verify -f - -- <<EOF
apiVersion: networking.gke.io/v1beta1
kind: ManagedCertificate
metadata:
  name: "${var.tenant_name}-crystal-io"
  namespace: "${var.tenant_name}"
spec:
  domains:
    - "${var.tenant_base_endpoint}"
EOF
EOT
  }
}

resource "null_resource" "minio-gcp-crystal-ai" {
  provisioner "local-exec" {
    command = <<EOT
kubectl apply --server=https://${data.terraform_remote_state.kubernetes.outputs.host} --username=${data.terraform_remote_state.kubernetes.outputs.username} --password=${data.google_secret_manager_secret_version.gke_admin_password.secret_data} --insecure-skip-tls-verify -f - -- <<EOF
apiVersion: networking.gke.io/v1beta1
kind: ManagedCertificate
metadata:
  name: "${var.tenant_name}-minio-gcp-crystal-ai"
  namespace: "${var.tenant_name}"
spec:
  domains:
    - "${var.tenant_minio_endpoint}-minio.gcp.crystal.ai"
EOF
EOT
  }
}

resource "google_compute_global_address" "public-ingress" {
  name = "${var.tenant_name}-public-ingress"
}

resource "google_compute_global_address" "minio-ingress" {
  name = "${var.tenant_name}-minio-ingress"
}

resource "kubernetes_ingress" "crystal-ingress" {
  depends_on = [
    null_resource.crystal-io,
    google_compute_global_address.public-ingress
  ]
  metadata {
    name      = "crystal-ingress"
    namespace = var.tenant_name
    annotations = {
      "kubernetes.io/ingress.global-static-ip-name" = google_compute_global_address.public-ingress.name
      "networking.gke.io/managed-certificates"      = "${var.tenant_name}-crystal-io"
    }
  }
  spec {
    backend {
      service_name = "proxy"
      service_port = 8080
    }
  }
}

resource "kubernetes_ingress" "minio-ingress" {
  depends_on = [
    null_resource.minio-gcp-crystal-ai,
    google_compute_global_address.minio-ingress
  ]
  metadata {
    name      = "minio-ingress"
    namespace = var.tenant_name
    annotations = {
      "kubernetes.io/ingress.global-static-ip-name" = google_compute_global_address.minio-ingress.name
      "networking.gke.io/managed-certificates"      = "${var.tenant_name}-minio-gcp-crystal-ai"
    }
  }
  spec {
    backend {
      service_name = "nlp-minio"
      service_port = 9000
    }
  }
}