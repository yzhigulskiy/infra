resource "kubernetes_service" "api-gateway" {
  depends_on = [
    kubernetes_deployment.api-gateway
  ]
  metadata {
    name      = "api-gateway"
    namespace = var.tenant_name
    labels = {
      app = "api-gateway"
    }
  }
  spec {
    selector = {
      app = "api-gateway"
    }
    port {
      name        = "http"
      protocol    = "TCP"
      port        = 8080
      target_port = 8080
    }
    port {
      name        = "http-admin"
      protocol    = "TCP"
      port        = 9876
      target_port = 9876
    }
    session_affinity = "None"
    type             = "ClusterIP"
  }
}

resource "kubernetes_config_map" "crystal-api-gateway-jwt-pem-cfgmap-6d57595k6t" {
  metadata {
    name      = "crystal-api-gateway-jwt-pem-cfgmap-6d57595k6t"
    namespace = var.tenant_name
  }
  data = {
    "jwt.pem" = file("${path.module}/templates/jwt.pem")
  }
}

data "template_file" "crystal-api-system-config-cfgmap" {
  template = file("${path.module}/templates/system.config.yml")
  vars = {
    tenant_name = var.tenant_name
  }
}

resource "kubernetes_config_map" "crystal-api-gateway-config-cfgmap-mf98t2mtbt" {
  metadata {
    name      = "crystal-api-gateway-config-cfgmap-mf98t2mtbt"
    namespace = var.tenant_name
  }
  data = {
    "system.config.yml"  = data.template_file.crystal-api-system-config-cfgmap.rendered
    "gateway.config.yml" = file("${path.module}/templates/gateway.config.yml")
  }
}

resource "kubernetes_deployment" "api-gateway" {
  depends_on = [
    kubernetes_config_map.crystal-api-gateway-jwt-pem-cfgmap-6d57595k6t
  ]
  metadata {
    name = "api-gateway"
    labels = {
      app = "api-gateway"
    }
    namespace = var.tenant_name
  }

  spec {
    replicas               = 1
    revision_history_limit = 10

    selector {
      match_labels = {
        app = "api-gateway"
      }
    }

    strategy {
      rolling_update {
        max_surge       = "50%"
        max_unavailable = "0"
      }
      type = "RollingUpdate"
    }

    template {
      metadata {
        labels = {
          app = "api-gateway"
        }
      }
      spec {
        container {
          image             = "eu.gcr.io/igenius-registry/crystal.api-gateway:v0.3.1"
          name              = "api-gateway"
          image_pull_policy = "Always"
          liveness_probe {
            failure_threshold = 3
            http_get {
              path   = "/api/healthcheck/"
              port   = 8080
              scheme = "HTTP"
            }
            initial_delay_seconds = 3
            period_seconds        = 3
            success_threshold     = 1
            timeout_seconds       = 1
          }
          volume_mount {
            mount_path = "/var/lib/eg/gateway.config.yml"
            name       = "config-volume"
            sub_path   = "gateway.config.yml"
          }
          volume_mount {
            mount_path = "/var/lib/eg/system.config.yml"
            name       = "config-volume"
            sub_path   = "system.config.yml"
          }
          volume_mount {
            mount_path = "/var/lib/eg/jwt.pem"
            name       = "jwt-pem-volume"
            sub_path   = "jwt.pem"
          }
        }
        termination_grace_period_seconds = 30
        volume {
          config_map {
            name = "crystal-api-gateway-jwt-pem-cfgmap-6d57595k6t"
          }
          name = "config-volume-jwt-key"
        }
        volume {
          config_map {
            items {
              key  = "gateway.config.yml"
              path = "gateway.config.yml"
            }
            items {
              key  = "system.config.yml"
              path = "system.config.yml"
            }
            name = "crystal-api-gateway-config-cfgmap-mf98t2mtbt"
          }
          name = "config-volume"
        }
        volume {
          config_map {
            items {
              key  = "jwt.pem"
              path = "jwt.pem"
            }
            name = "crystal-api-gateway-jwt-pem-cfgmap-6d57595k6t"
          }
          name = "jwt-pem-volume"
        }
      }
    }
  }
}

data "template_file" "jwttopem-script" {
  template = file("${path.module}/templates/script.sh")
}

resource "kubernetes_config_map" jwttopem {
  metadata {
    name      = "jwttopem"
    namespace = var.tenant_name
  }
  data = {
    "script.sh" = data.template_file.jwttopem-script.rendered
  }
}

resource "kubernetes_service_account" "jwttopem" {
  metadata {
    name      = "jwttopem"
    namespace = var.tenant_name
  }
  image_pull_secret {
    name = "gcp-registry"
  }
}

resource kubernetes_role "jwttopem" {
  depends_on = [
    kubernetes_service_account.jwttopem
  ]
  metadata {
    name      = "jwttopem"
    namespace = var.tenant_name
  }
  rule {
    api_groups = ["", "apps"]
    resources  = ["configmaps", "deployments", "deployments/scale"]
    verbs      = ["get", "update", "patch"]
  }
}

resource "kubernetes_role_binding" "jwttopem" {
  depends_on = [
    kubernetes_service_account.jwttopem,
    kubernetes_role.jwttopem
  ]
  metadata {
    name      = "jwttopem"
    namespace = var.tenant_name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "jwttopem"
  }
  subject {
    kind = "ServiceAccount"
    name = "jwttopem"
    //    api_group = "rbac.authorization.k8s.io"
    namespace = var.tenant_name
  }
}

resource "kubernetes_job" "jwttopem" {
  depends_on = [
    kubernetes_config_map.jwttopem,
    kubernetes_deployment.api-gateway,
    kubernetes_service.api-gateway,
    kubernetes_role_binding.jwttopem
  ]
  metadata {
    name      = "jwttopem"
    namespace = var.tenant_name
  }
  spec {
    backoff_limit = 10
    template {
      metadata {}
      spec {
        service_account_name            = "jwttopem"
        automount_service_account_token = true
        restart_policy                  = "OnFailure"
        container {
          name    = "jwttopem"
          image   = "eu.gcr.io/igenius-registry/kubectl-proxy:0.0.1"
          command = ["sh"]
          args    = ["-x", "/opt/script.sh"]
          volume_mount {
            mount_path = "/opt"
            name       = "script"
          }
        }
        volume {
          name = "script"
          config_map {
            name         = "jwttopem"
            default_mode = "0755"
            items {
              key  = "script.sh"
              path = "script.sh"
            }
          }
        }
      }
    }
  }
}

//resource "kubernetes_service" "console-i18n-dictionaries" {
//  metadata {
//    name = "console-i18n-dictionaries"
//    namespace = var.tenant_name
//    labels = {
//      app = "console-i18n-dictionaries"
//    }
//  }
//  spec {
//    selector = {
//      app = "console-i18n-dictionaries"
//    }
//    port {
//      port        = 8080
//      target_port = 80
//    }
//    session_affinity = "None"
//    type = "ClusterIP"
//  }
//}
//
//resource "kubernetes_deployment" "console-i18n-dictionaries" {
//  metadata {
//    name = "console-i18n-dictionaries"
//    namespace = var.tenant_name
//    labels = {
//      app = "console-i18n-dictionaries"
//    }
//  }
//  spec {
//    selector {
//      match_labels = {
//        app = "console-i18n-dictionaries"
//      }
//    }
//    template {
//      metadata {
//        labels = {
//          app = "console-i18n-dictionaries"
//        }
//      }
//      spec {
//        container {
//          image = "eu.gcr.io/igenius-registry/console.i18n-dictionaries:${var.crystal_console_i18n_dictionaries}"
//          name  = "console-i18n-dictionaries"
//        }
//      }
//    }
//  }
//}
//
//resource "kubernetes_service" "i18n-dictionaries" {
//  metadata {
//    name = "i18n-dictionaries"
//    namespace = var.tenant_name
//  }
//  spec {
//    selector = {
//      app = "i18n-dictionaries"
//    }
//    port {
//      port        = 80
//      protocol    = "TCP"
//      target_port = 80
//    }
//    session_affinity = "None"
//    type = "ClusterIP"
//  }
//}
//
//resource "kubernetes_deployment" "i18n-dictionaries" {
//  metadata {
//    name = "i18n-dictionaries"
//    namespace = var.tenant_name
//    labels = {
//      app = "i18n-dictionaries"
//    }
//  }
//  spec {
//    replicas = 1
//    revision_history_limit = 10
//    selector {
//      match_labels = {
//        app = "i18n-dictionaries"
//      }
//    }
//    strategy {
//      rolling_update {
//        max_surge = "50%"
//        max_unavailable = "0"
//      }
//      type = "RollingUpdate"
//    }
//    template {
//
//      metadata {
//        labels = {
//          app = "i18n-dictionaries"
//        }
//      }
//      spec {
//        container {
//          image = "eu.gcr.io/igenius-registry/gsk.i18n-dictionaries:v0.1.0"
//          name  = "i18n-dictionaries"
//          image_pull_policy = "Always"
//          liveness_probe {
//            failure_threshold = 3
//            http_get {
//              path = "/healthcheck"
//              port = "80"
//              scheme = "HTTP"
//            }
//            initial_delay_seconds = 5
//            period_seconds = 30
//            timeout_seconds = 1
//            success_threshold = 1
//          }
//        }
//      }
//    }
//  }
//}

resource "kubernetes_service" "pods-version-reader" {
  metadata {
    labels = {
      app = "pods-version-reader"
    }
    name      = "pods-version-reader"
    namespace = var.tenant_name
  }
  spec {
    selector = {
      app = "pods-version-reader"
    }
    port {
      port        = 8080
      protocol    = "TCP"
      target_port = 8080
    }
    session_affinity = "None"
    type             = "ClusterIP"
  }
}

resource "kubernetes_service_account" "igenius-pod-reader-sa" {
  metadata {
    labels = {
      app = "pods-version-reader"
    }
    name      = "igenius-pod-reader-sa"
    namespace = var.tenant_name
  }
}

resource "kubernetes_role" "igenius-can-read-pods" {
  metadata {
    namespace = var.tenant_name
    name      = "igenius-can-read-pods"
    labels = {
      app = "pods-version-reader"
    }
  }
  rule {
    api_groups = [""]
    resources  = ["pods"]
    verbs      = ["get", "list"]
  }
  rule {
    api_groups = ["apps"]
    resources  = ["deployments"]
    verbs      = ["get", "list"]
  }
}

resource "kubernetes_role_binding" "igenius-can-read-pods-binding" {
  metadata {
    labels = {
      app = "pods-version-reader"
    }
    name      = "igenius-can-read-pods-binding"
    namespace = var.tenant_name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "igenius-can-read-pods"
  }
  subject {
    kind = "ServiceAccount"
    name = "igenius-pod-reader-sa"
  }
}

resource "kubernetes_deployment" "pods-version-reader" {
  metadata {
    name      = "pods-version-reader"
    namespace = var.tenant_name
    labels = {
      app = "pods-version-reader"
    }
  }
  spec {
    replicas               = 1
    revision_history_limit = 10
    selector {
      match_labels = {
        app = "pods-version-reader"
      }
    }
    strategy {
      rolling_update {
        max_surge       = "50%"
        max_unavailable = "0"
      }
      type = "RollingUpdate"
    }
    template {
      metadata {
        labels = {
          app = "pods-version-reader"
        }
      }
      spec {
        container {
          image             = "eu.gcr.io/igenius-registry/igenius.pods-version-reader:v0.0.1"
          name              = "pods-version-reader"
          image_pull_policy = "IfNotPresent"
          liveness_probe {
            http_get {
              path   = "/health"
              port   = "8080"
              scheme = "HTTP"
            }
            initial_delay_seconds = 5
            period_seconds        = 30
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "proxy" {
  metadata {
    labels = {
      app = "proxy"
    }
    name      = "proxy"
    namespace = var.tenant_name
  }
  spec {
    selector = {
      app = "proxy"
    }
    port {
      name        = "proxy-https"
      port        = 8080
      protocol    = "TCP"
      target_port = 8080
    }
    session_affinity = "None"
    type             = "NodePort"
  }
}

data "template_file" "crystal-proxy-conf-cfgmap-fffchgg5b7" {
  template = file("${path.module}/templates/default.conf")
  vars = {
    tenant_base_endpoint = var.tenant_base_endpoint
  }
}

resource "kubernetes_config_map" "crystal-proxy-conf-cfgmap-fffchgg5b7" {
  metadata {
    name      = "crystal-proxy-conf-cfgmap-fffchgg5b7"
    namespace = var.tenant_name
  }
  data = {
    "default.conf" = data.template_file.crystal-proxy-conf-cfgmap-fffchgg5b7.rendered
  }
}

resource "kubernetes_deployment" "proxy" {
  metadata {
    name      = "proxy"
    namespace = var.tenant_name
    labels = {
      app = "proxy"
    }
  }
  spec {
    replicas               = 1
    revision_history_limit = 10
    selector {
      match_labels = {
        app = "proxy"
      }
    }
    strategy {
      rolling_update {
        max_surge       = "25%"
        max_unavailable = "25%"
      }
      type = "RollingUpdate"
    }
    template {
      metadata {
        labels = {
          app = "proxy"
        }
      }
      spec {
        container {
          image             = "nginx:1.17-alpine"
          name              = "proxy"
          image_pull_policy = "IfNotPresent"
          volume_mount {
            mount_path = "/etc/nginx/conf.d/"
            name       = "proxy-conf-volume"
          }
        }
        volume {
          config_map {
            name = "crystal-proxy-conf-cfgmap-fffchgg5b7"
          }
          name = "proxy-conf-volume"
        }
      }
    }
  }
}

resource "kubernetes_service" "rene-en" {
  metadata {
    labels = {
      app = "rene-en"
    }
    name      = "rene-en"
    namespace = var.tenant_name
  }
  spec {
    selector = {
      app = "rene-en"
    }
    port {
      port        = 8080
      protocol    = "TCP"
      target_port = 50210
    }
    session_affinity = "None"
    type             = "ClusterIP"
  }
}

resource "kubernetes_deployment" "rene-en" {
  metadata {
    name      = "rene-en"
    namespace = var.tenant_name
    labels = {
      app = "rene-en"
    }
  }
  spec {
    replicas               = 1
    revision_history_limit = 10
    selector {
      match_labels = {
        app = "rene-en"
      }
    }
    strategy {
      rolling_update {
        max_surge       = "50%"
        max_unavailable = "0"
      }
      type = "RollingUpdate"
    }
    template {
      metadata {
        labels = {
          app = "rene-en"
        }
      }
      spec {
        container {
          image             = "eu.gcr.io/igenius-registry/gsk.rene-en:v0.0.1"
          name              = "rene"
          image_pull_policy = "Always"
        }
        termination_grace_period_seconds = 30
      }
    }
  }
}

resource "kubernetes_service" "speech-service-auth" {
  metadata {
    labels = {
      app = "speech-service-auth"
    }
    name      = "speech-service-auth"
    namespace = var.tenant_name
  }
  spec {
    selector = {
      app = "speech-service-auth"
    }
    port {
      port        = 8080
      protocol    = "TCP"
      target_port = 8080
    }
    session_affinity = "None"
    type             = "ClusterIP"
  }
}

resource "kubernetes_deployment" "tiny-tools" {
  metadata {
    name      = "tiny-tools"
    namespace = var.tenant_name
    labels = {
      app = "tiny-tools"
    }
  }
  spec {
    replicas               = 1
    revision_history_limit = 3
    selector {
      match_labels = {
        app = "tiny-tools"
      }
    }
    strategy {
      rolling_update {
        max_surge       = "50%"
        max_unavailable = "0"
      }
      type = "RollingUpdate"
    }
    template {
      metadata {
        labels = {
          app = "tiny-tools"
        }
      }
      spec {
        container {
          command           = ["/bin/sh", "-c", "while true; do sleep 120; done"]
          image             = "giantswarm/tiny-tools"
          name              = "tiny-tools"
          image_pull_policy = "IfNotPresent"
        }
      }
    }
  }
}

resource "kubernetes_service" "web-assets" {
  metadata {
    labels = {
      app = "web-assets"
    }
    name      = "web-assets"
    namespace = var.tenant_name
  }
  spec {
    selector = {
      app = "web-assets"
    }
    port {
      port        = 80
      protocol    = "TCP"
      target_port = 80
    }
    type = "ClusterIP"
  }
}

resource "kubernetes_deployment" "web-assets" {
  metadata {
    name      = "web-assets"
    namespace = var.tenant_name
    labels = {
      app = "web-assets"
    }
  }
  spec {
    replicas               = 1
    revision_history_limit = 10
    selector {
      match_labels = {
        app = "web-assets"
      }
    }
    strategy {
      rolling_update {
        max_surge       = "50%"
        max_unavailable = "0"
      }
      type = "RollingUpdate"
    }
    template {
      metadata {
        labels = {
          app = "web-assets"
        }
      }
      spec {
        container {
          image             = "eu.gcr.io/igenius-registry/crystal.assets:production"
          name              = "web-assets"
          image_pull_policy = "Always"
        }
        termination_grace_period_seconds = 30
      }
    }
  }
}

resource "kubernetes_service" "widgets" {
  metadata {
    labels = {
      app = "widgets"
    }
    name      = "widgets"
    namespace = var.tenant_name
  }
  spec {
    selector = {
      app = "widgets"
    }
    port {
      port        = 8080
      protocol    = "TCP"
      target_port = "8080"
    }
    session_affinity = "None"
    type             = "ClusterIP"
  }
}

resource "kubernetes_deployment" "widgets" {
  metadata {
    name      = "widgets"
    namespace = var.tenant_name
    labels = {
      app = "widgets"
    }
  }
  spec {
    replicas               = 1
    revision_history_limit = 10
    selector {
      match_labels = {
        app = "widgets"
      }
    }
    strategy {
      rolling_update {
        max_surge       = "50%"
        max_unavailable = "0"
      }
      type = "RollingUpdate"
    }
    template {
      metadata {
        labels = {
          app = "widgets"
        }
      }
      spec {
        container {
          env {
            name  = "SERVICE_NAME"
            value = "widgets"
          }
          env {
            name  = "WIDGETS_CONFIGURATION_PATH"
            value = "/gsk/widgets_configuration"
          }
          env {
            name  = "DATA_SOURCE_ADAPTER_URL"
            value = "http://data-source-adapter:8080"
          }
          env {
            name  = "ENV"
            value = "PROD"
          }
          image             = "eu.gcr.io/igenius-registry/gsk.widgets:latest"
          name              = "widgets"
          image_pull_policy = "Always"
        }
        termination_grace_period_seconds = 30
        volume {
          config_map {
            items {
              key  = "config.json"
              path = "config.json"
            }
            items {
              key  = "apple-app-site-association"
              path = "apple-app-site-association"
            }
            items {
              key  = "assetlinks.json"
              path = "assetlinks.json"
            }
            name = "crystal-webapp-config-cfgmap-8c5m2h9bgh"
          }
          name = "config-volume"
        }
      }
    }
  }
}

resource "kubernetes_service" "widget-export" {
  metadata {
    labels = {
      app = "widget-export"
    }
    name      = "widget-export"
    namespace = var.tenant_name
  }
  spec {
    selector = {
      app = "widget-export"
    }
    port {
      port        = 8080
      protocol    = "TCP"
      target_port = "8080"
    }
    session_affinity = "None"
    type             = "ClusterIP"
  }
}

resource "kubernetes_deployment" "widget-export" {
  metadata {
    name      = "widget-export"
    namespace = var.tenant_name
    labels = {
      app = "widget-export"
    }
  }
  spec {
    replicas               = 1
    revision_history_limit = 10
    selector {
      match_labels = {
        app = "widget-export"
      }
    }
    strategy {
      rolling_update {
        max_surge       = "50%"
        max_unavailable = "0"
      }
      type = "RollingUpdate"
    }
    template {
      metadata {
        labels = {
          app = "widget-export"
        }
      }
      spec {
        container {
          env {
            name  = "WEB_ASSETS_SERVICE_URL"
            value = "http://crystal-web-assets"
          }
          env {
            name  = "I18N_DICTIONARIES_URL"
            value = "http://i18n-dictionaries"
          }
          image             = "eu.gcr.io/igenius-registry/crystal.widget-export:v4.0.3"
          name              = "widget-export"
          image_pull_policy = "Always"
        }
      }
    }
  }
}