#!/usr/bin/env sh
set -e

# FUNCTIONS
decodeBase64UrlUInt() { #input:base64UrlUnsignedInteger
    local binaryDigits paddedStr
    case $(( $${#1} % 4 )) in
        2) paddedStr="$1=="   ;;
        3) paddedStr="$1="    ;;
        *) paddedStr="$1"     ;;
    esac
    binaryDigits=$(             \
        echo -n "$paddedStr"    \
        | tr '_-' '/+'          \
        | openssl enc -d -a -A  \
        | xxd -b -g 0           \
        | cut -d ' ' -f 2       \
        | paste -s -d ''        \
    )
}

base64UrlToHex() { #input:base64UrlString
    local hexStr paddedStr
    case $(( $${#1} % 4 )) in
        2) paddedStr="$1=="   ;;
        3) paddedStr="$1="    ;;
        *) paddedStr="$1"     ;;
    esac
    hexStr=$(                   \
        echo -n "$paddedStr"    \
        | tr '_-' '/+'          \
        | base64 -d             \
        | xxd -p -u             \
        | tr -d '\n'            \
    )
    echo "$hexStr"
}

asn1Conf() { #input:hexStrPlainUpperCase
    local e="$1"
    local n="$2"
    echo "apiVersion: v1
kind: ConfigMap
metadata:
  name: crystal-api-gateway-jwt-pem-cfgmap-6d57595k6t
data:
  jwt.pem: |-" > /tmp/jwt.yaml

    echo "
        asn1 = SEQUENCE:pubkeyinfo
        [pubkeyinfo]
        algorithm = SEQUENCE:rsa_alg
        pubkey = BITWRAP,SEQUENCE:rsapubkey
        [rsa_alg]
        algorithm = OID:rsaEncryption
        parameter = NULL
        [rsapubkey]
        n = INTEGER:0x$n
        e = INTEGER:0x$e
    " | sed '/^$/d ; s/^ *//g'              \
    | openssl asn1parse                     \
        -genconf    /dev/stdin              \
        -out        /tmp/asn.str
    openssl rsa                           \
        -pubin                              \
            -inform     DER                 \
            -outform    PEM                 \
            -in         /tmp/asn.str          \
            -out        /tmp/jwt.cert
#    sed -i -e 's/^/    /' /tmp/jwt.cert
    sed -i 's/^[ \t]*/    /;s/[ \t]*$/    /' /tmp/jwt.cert
    cat /tmp/jwt.cert >> /tmp/jwt.yaml
    /kubectl apply -f /tmp/jwt.yaml
    /kubectl scale deployment api-gateway --replicas=0
    /kubectl scale deployment api-gateway --replicas=1
    }

main() {
    local e n hexArr
    local jwksUrl="$1"

    while true; do
      jwkJson=$(curl -sSSLk http://api-gateway:8080/iam/.well-known/jwks.json)
        if [[ "$jwkJson" == "Bad gateway." ]]; then
          echo "Api not ready. continue looping"
          sleep 3
        else
    	    break
        fi
    done

    local jwkJson=$(curl -sSSLk $jwksUrl || exit 1)
    local kidList=$(echo "$jwkJson" | jq -r '.keys[0].kid')
    for keyId in $kidList; do
        n=$(echo "$jwkJson" | jq -r ".keys[] | select(.kid == \"$keyId\") | .n")
        e=$(echo "$jwkJson" | jq -r ".keys[] | select(.kid == \"$keyId\") | .e")
        asn1Conf $(base64UrlToHex "$e") $(base64UrlToHex "$n")
    done
}

# MAIN
ls -l /var/run/secrets/kubernetes.io/serviceaccount/
API_SERVER="https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_SERVICE_PORT"
CA_CRT="/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
TOKEN="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)"
/kubectl proxy --server="$API_SERVER" --certificate-authority="$CA_CRT" --token="$TOKEN" --accept-paths='^.*' &

main 'http://api-gateway:8080/iam/.well-known/jwks.json'