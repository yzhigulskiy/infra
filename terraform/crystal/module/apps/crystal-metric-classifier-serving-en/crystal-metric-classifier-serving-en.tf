data "template_file" "crystal-metric-classifier-serving-en-values" {
  template = file("${path.module}/../../templates/crystal-metric-classifier-serving-en-develop.yaml")
  vars = {
    crystal_metric_classifier_serving_en_serve      = var.crystal_metric_classifier_serving_en_serve
    crystal_metric_classifier_serving_en_controller = var.crystal_metric_classifier_serving_en_controller
  }
}

resource "helm_release" "crystal-metric-classifier-serving-en" {
  depends_on = [
    data.template_file.crystal-metric-classifier-serving-en-values
  ]
  timeout    = 600
  name       = "metric-classifier-en"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-metric-classifier-serving-en-values.rendered
  ]
}