data "template_file" "crystal-topic-widget-values" {
  template = file("${path.module}/../../templates/crystal-topic-widget-service-values-develop.yaml")
  vars = {
    crystal_topic_widget_service = var.crystal_topic_widget_service
  }
}

resource "helm_release" "crystal-topic-widget" {
  depends_on = [
    data.template_file.crystal-topic-widget-values
  ]
  timeout    = 600
  name       = "topic-widget"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.crystal-topic-widget-values.rendered
  ]
}