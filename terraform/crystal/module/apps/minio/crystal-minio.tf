data "template_file" "minio-values" {
  template = file("${path.module}/../../templates/crystal-minio-values-develop.yaml")
  vars = {
    crystal_minio_minio = var.crystal_minio_minio
    crystal_minio_mc    = var.crystal_minio_mc
  }
}

resource "helm_release" "minio" {
  depends_on = [
    data.template_file.minio-values
  ]
  timeout    = 600
  name       = "nlp-minio"
  chart      = var.helm_release_name
  repository = var.helm_repository_url
  namespace  = var.tenant_name
  values = [
    data.template_file.minio-values.rendered
  ]
}