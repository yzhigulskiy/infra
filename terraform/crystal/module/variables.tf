variable "REGION" {
  type        = string
  description = "GCP region used for all resources"
  default     = "europe-west3"
}
variable "PROJECT" {
  type = string
  description = "GCP Project ID"
}
variable "cloudsql_tier" {
}
variable "users" {
  description = "User for SQL Instance"
}
variable "master_authorized_networks" {
  description = "List of master authorized networks. If none are provided, disallow external access (except the cluster node IPs, which GKE automatically whitelists)."
  default     = []
}
variable "tenant_name" {}
variable "network_id" {}
variable "tenant_base_endpoint" {}
variable "dataset_template_bucket" {}
variable "tenant_minio_endpoint" {}
variable "retrain_nats_endpoint" {}
variable "service_versions" {}
variable "deletion_protection" {}
