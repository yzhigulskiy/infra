{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "common.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "common.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}


{{/*- define "common.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name "test"| trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name .Values.nameOverride "test"| trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -*/}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "common.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
Adding complition with all the standard labels: https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/#labels
*/}}
{{- define "common.labels" -}}
helm.sh/chart: {{ include "common.chart" . }}
{{ include "common.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "common.selectorLabels" -}}
app.kubernetes.io/name: {{ include "common.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{/*
Create the name of the service account to use
TO-DO: Refactor without serviceAccount.create
*/}}
{{- define "common.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "common.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Generate service config data for configmap stored in service-config-storage
*/}}
{{/* - define "common.serviceConfigmap" -}}
{{- $service := index .Values "service-config" "config" -}}
{{- $config := unset $service "secrets" -}}
{{- toYaml $config -}}
{{- end -*/}}

{{/*
Generate image tag for db migration pre-installation-hook
TO-DO: Refactor in the same way of the "common.testImageName"
TO-DO: Define microservice.repository as default
*/}}
{{- define "common.migrationImageName" -}}
{{- if .Values.migration.name -}}
   {{ .Values.migration.name }}
{{- else -}}
   {{ .Values.microservice.image.name }}-migrations
{{- end -}}
{{- end -}}

{{/*
Generate image tag for e2e tests
TO-DO: Define microservice.repository as default
*/}}
{{- define "common.testImageName" -}}
{{- if index .Values "service-test" "name" -}}
   {{ index .Values "service-test" "repository" }}/{{ index .Values "service-test" "name" }}:{{ index .Values "service-test" "tag" }}
{{- else -}}
   {{ index .Values "service-test" "repository" }}/{{ .Values.microservice.image.name }}-tests:{{ index .Values "service-test" "tag" }}
{{- end -}}
{{- end -}}