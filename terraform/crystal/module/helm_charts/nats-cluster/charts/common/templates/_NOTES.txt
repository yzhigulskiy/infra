{{- define "common.notes" -}}
{{- $common := dict "Values" .Values.common -}}
{{- $noCommon := omit .Values "common" -}}
{{- $overrides := dict "Values" $noCommon -}}
{{- $noValues := omit . "Values" -}}
{{- with merge $noValues $overrides $common -}}

1. Watch all pods come up:
  $ kubectl get pods --namespace={{ .Release.Namespace }} -w | grep {{ .Release.Name }}
2. Test microservice health using Helm test:
  $ helm test {{ template "common.fullname" . }}
3. (Optional) Manually confirm {{ template "common.fullname" . }} is healthy:
  $ kubectl --namespace={{ .Release.Namespace }} port-forward svc/{{ template "common.fullname" . }}-svc 8080:8080
  $ http http://localhost:8080/healthcheck
4. (Optional) Manually check {{ template "common.fullname" . }} logs:
  $ kubectl --namespace={{ .Release.Namespace }} logs svc/{{ template "common.fullname" . }}-svc -f

{{- end -}}
{{- end -}}