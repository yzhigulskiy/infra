{{/*
Expand the name of the chart.
*/}}
{{- define "util.name.tpl" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "util.chart.tpl" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{/*
Common labels
Adding complition with all the standard labels: https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/#labels
helm.sh/chart:
app.kubernetes.io/version:
app.kubernetes.io/managed-by:
app.kubernetes.io/name:
app.kubernetes.io/instance:


app.kubernetes.io/name: mysql
app.kubernetes.io/instance: crystal/train
app.kubernetes.io/version: "5.7.21"
app.kubernetes.io/component: nats/stan
app.kubernetes.io/part-of: nlpg
app.kubernetes.io/managed-by: helm
*/}}
{{- define "util.labels.tpl" -}}
helm.sh/chart: {{ include "util.chart.tpl" . }}
{{ include "util.selectorLabels.tpl" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "util.selectorLabels.tpl" -}}
app.kubernetes.io/name: {{ include "util.name.tpl" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/component: nats
app.kubernetes.io/part-of: nlpg
{{- end -}}