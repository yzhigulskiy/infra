{{- define "stan-monitoring.annotations.tpl" -}}
prometheus.io/scrape: "true"
prometheus.io/port: "7777"
{{- end -}}

{{- define "stan-monitoring.container.tpl" -}}
name: "metrics"
image: {{ .Values.cluster.natsStreamingConfig.monitoring.image | default "synadia/prometheus-nats-exporter" }}:{{ .Values.cluster.natsStreamingConfig.monitoring.tag | default "0.6.0" }}
args: ["-varz", "-channelz", "-serverz", "http://localhost:8222"]
ports:
- name: "metrics"
  containerPort: 7777
  protocol: TCP
{{- end -}}