{{- define "stan-storage.container.tpl" -}}
name: nats-streaming
volumeMounts:
- mountPath: /etc/stan/config
  name: stan-secret
  readOnly: true
{{- end -}}

{{- define "stan-storage.container.volume.tpl" -}}
name: stan-secret
secret:
  secretName: {{ .Values.cluster.name }}-stan-sct
{{- end -}}

{{- define "stan-storage.container.secret.tpl" -}}
streaming: {
  sql: {
    driver: "postgres"
    source: {{ .Values.cluster.natsStreamingConfig.PG_URL | quote }}
   }
}
{{- end -}}