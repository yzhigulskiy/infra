{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}

{{- define "cluster.size" -}}
  {{- if eq .environment "local" -}}
   {{ printf "%s" "1" }}
  {{- else -}}
   {{ .size }}
  {{- end -}}
{{- end -}}
