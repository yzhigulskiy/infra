data "terraform_remote_state" "kubernetes" {
  backend = "gcs"
  config = {
    bucket = "tf-state-${var.PROJECT}"
    prefix = "kubernetes/state"
  }
}

data "google_secret_manager_secret_version" "gke_admin_password" {
  secret  = "gke_admin_password"
  version = "1"
}

provider "helm" {
  kubernetes {
    host                   = "https://${data.terraform_remote_state.kubernetes.outputs.host}"
    username               = data.terraform_remote_state.kubernetes.outputs.username
    password               = data.google_secret_manager_secret_version.gke_admin_password.secret_data
    cluster_ca_certificate = data.terraform_remote_state.kubernetes.outputs.cluster_ca_certificate
    load_config_file       = false
  }
}

provider "kubernetes" {
  host                   = "https://${data.terraform_remote_state.kubernetes.outputs.host}"
  username               = data.terraform_remote_state.kubernetes.outputs.username
  password               = data.google_secret_manager_secret_version.gke_admin_password.secret_data
  cluster_ca_certificate = data.terraform_remote_state.kubernetes.outputs.cluster_ca_certificate
  load_config_file       = false
}

data "google_secret_manager_secret_version" "gcp-registry" {
  secret  = "gcp-registry"
  version = "1"
}

module "cloudsql" {
  source                     = "git@git.igenius.net:devops/terraform/modules/cloudsql.git?ref=0.0.15"
  instance_name              = "db"
  region                     = var.REGION
  project                    = var.PROJECT
  tier                       = var.cloudsql_tier
  network_id                 = var.network_id
  db_name                    = var.tenant_name
  additional_users           = var.users
  master_authorized_networks = var.master_authorized_networks
  tenant_name                = var.tenant_name
  deletion_protection        = var.deletion_protection
}

module "sql-provision" {
  source                     = "git@git.igenius.net:devops/terraform/modules/sql-provision.git?ref=0.0.16"
  cloud_sql_additional_users = module.cloudsql.sql_additional_users
  cloud_sql_databases        = module.cloudsql.sql_database
  kubernetes_secrets         = var.users
  helm_repository_name       = "igenius-charts"
  helm_repository_url        = "gs://igenius-charts"
  helm_chart_name            = "crystal-sql-provision"
  helm_crystal_sql_ip        = module.cloudsql.google_sql_database_ip_address
  GCP_REGISTRY_USER          = data.google_secret_manager_secret_version.gcp-registry.secret_data
  gcp_registry_username      = "gcp-registry"
  tenant_name                = var.tenant_name
  PROJECT                    = var.PROJECT
}

module "login-manager" {
  depends_on                      = [module.sql-provision, module.cloudsql, module.zipkin]
  source                          = "./apps/login-manager"
  helm_repository_url             = "gs://igenius-charts"
  helm_release_name               = "crystal-login-manager"
  cloud_sql_database_name         = module.cloudsql.sql_database_name
  cloud_sql_database_ip           = module.cloudsql.google_sql_database_ip_address
  tenant_name                     = var.tenant_name
  tenant_base_endpoint            = var.tenant_base_endpoint
  crystal_login_manager           = var.service_versions["crystal_login_manager"]
  crystal_login_manager_migration = var.service_versions["crystal_login_manager_migration"]
}

module "nats" {
  depends_on              = [module.login-manager, module.sql-provision]
  source                  = "./apps/nats"
  helm_repository_url     = "gs://igenius-charts/infra"
  cloud_sql_database_name = module.cloudsql.sql_database_name
  cloud_sql_database_ip   = module.cloudsql.google_sql_database_ip_address
  GCP_REGISTRY_USER       = data.google_secret_manager_secret_version.gcp-registry.secret_data
  gcp_registry_username   = "gcp-registry"
  tenant_name             = var.tenant_name
  crystal_nats_migration  = var.service_versions["crystal_nats_migration"]
}

module "etcd" {
  depends_on           = [module.login-manager, module.sql-provision]
  source               = "./apps/etcd"
  helm_repository_url  = "gs://igenius-charts/infra"
  helm_release_name    = "etcd"
  tenant_name          = var.tenant_name
  crystal_etcd_cluster = var.service_versions["crystal_etcd_cluster"]
}

module "hydra" {
  depends_on              = [module.login-manager, module.sql-provision]
  source                  = "./apps/hydra"
  helm_repository_url     = "gs://igenius-charts/infra"
  helm_release_name       = "hydra"
  cloud_sql_database_name = module.cloudsql.sql_database_name
  cloud_sql_database_ip   = module.cloudsql.google_sql_database_ip_address
  tenant_name             = var.tenant_name
  tenant_base_endpoint    = var.tenant_base_endpoint
  hydra                   = var.service_versions["hydra"]
}

module "keto" {
  depends_on              = [module.sql-provision, module.login-manager]
  source                  = "./apps/keto"
  helm_repository_url     = "gs://igenius-charts/infra"
  helm_release_name       = "keto"
  cloud_sql_database_name = module.cloudsql.sql_database_name
  cloud_sql_database_ip   = module.cloudsql.google_sql_database_ip_address
  tenant_name             = var.tenant_name
  keto                    = var.service_versions["keto"]
}

module "minio" {
  depends_on = [
    module.sql-provision,
    module.login-manager
  ]
  source              = "./apps/minio"
  helm_repository_url = "gs://igenius-charts/infra"
  helm_release_name   = "minio"
  tenant_name         = var.tenant_name
  crystal_minio_minio = var.service_versions["crystal_minio_minio"]
  crystal_minio_mc    = var.service_versions["crystal_minio_mc"]
}

module "advisor-manager" {
  depends_on = [
    module.sql-provision,
    module.login-manager
  ]
  source                  = "./apps/advisor-manager"
  helm_repository_url     = "gs://igenius-charts"
  helm_release_name       = "crystal-advisor-manager"
  cloud_sql_database_name = module.cloudsql.sql_database_name
  cloud_sql_database_ip   = module.cloudsql.google_sql_database_ip_address
  tenant_name             = var.tenant_name
  dataset_template_bucket = var.dataset_template_bucket
  retrain_nats_endpoint   = var.retrain_nats_endpoint
  crystal_advisor_manager = var.service_versions["crystal_advisor_manager"]
}

module "crystal-nlp-entities-manager" {
  depends_on                             = [module.sql-provision, module.login-manager, module.nats]
  source                                 = "./apps/crystal-nlp-entities-manager"
  helm_repository_url                    = "gs://igenius-charts"
  helm_release_name                      = "crystal-nlp-entities-manager"
  cloud_sql_database_name                = module.cloudsql.sql_database_name
  cloud_sql_database_ip                  = module.cloudsql.google_sql_database_ip_address
  tenant_name                            = var.tenant_name
  crystal_nlp_entities_manager           = var.service_versions["crystal_nlp_entities_manager"]
  crystal_nlp_entities_manager_migration = var.service_versions["crystal_nlp_entities_manager_migration"]
}

module "crystal-settings-service" {
  depends_on                         = [module.sql-provision, module.login-manager]
  source                             = "./apps/crystal-settings-service"
  helm_repository_url                = "gs://igenius-charts"
  helm_release_name                  = "crystal-settings-service"
  cloud_sql_database_name            = module.cloudsql.sql_database_name
  cloud_sql_database_ip              = module.cloudsql.google_sql_database_ip_address
  tenant_name                        = var.tenant_name
  tenant_base_endpoint               = var.tenant_base_endpoint
  crystal_settings_service           = var.service_versions["crystal_settings_service"]
  crystal_settings_service_migration = var.service_versions["crystal_settings_service_migration"]
}

module "crystal-alerting-squad" {
  depends_on = [
    module.sql-provision,
    module.login-manager
  ]
  source                           = "./apps/crystal-alerting-squad"
  helm_repository_url              = "gs://igenius-charts"
  helm_release_name                = "crystal-alerting"
  cloud_sql_database_name          = module.cloudsql.sql_database_name
  cloud_sql_database_ip            = module.cloudsql.google_sql_database_ip_address
  tenant_name                      = var.tenant_name
  crystal_alerting_squad           = var.service_versions["crystal_alerting_squad"]
  crystal_alerting_squad_migration = var.service_versions["crystal_alerting_squad_migration"]
}

module "aggregation-classifier-it" {
  depends_on = [
    module.sql-provision,
    module.login-manager
  ]
  source                            = "./apps/aggregation-classifier-it"
  helm_repository_url               = "gs://igenius-charts"
  helm_release_name                 = "crystal-aggregation-classifier-it"
  cloud_sql_database_name           = module.cloudsql.sql_database_name
  cloud_sql_database_ip             = module.cloudsql.google_sql_database_ip_address
  tenant_name                       = var.tenant_name
  crystal_aggregation_classifier_it = var.service_versions["crystal_aggregation_classifier_it"]
}

module "aggregation-classifier-en" {
  depends_on = [
    module.sql-provision,
    module.login-manager
  ]
  source                            = "./apps/aggregation-classifier-en"
  helm_repository_url               = "gs://igenius-charts"
  helm_release_name                 = "crystal-aggregation-classifier-en"
  tenant_name                       = var.tenant_name
  crystal_aggregation_classifier_en = var.service_versions["crystal_aggregation_classifier_en"]
}

module "crystal-alerting-adaptive-model-serving" {
  depends_on = [
    module.sql-provision,
    module.login-manager
  ]
  source                                  = "./apps/crystal-alerting-adaptive-model-serving"
  helm_repository_url                     = "gs://igenius-charts"
  helm_release_name                       = "crystal-alerting-adaptive-model-serving"
  tenant_name                             = var.tenant_name
  crystal_alerting_adaptive_model_serving = var.service_versions["crystal_alerting_adaptive_model_serving"]
}

module "crystal-datasource-adapter-mssql" {
  depends_on                       = [module.sql-provision, module.login-manager]
  source                           = "./apps/crystal-datasource-adapter-mssql"
  helm_repository_url              = "gs://igenius-charts"
  helm_release_name                = "crystal-datasource-adapter-mssql"
  tenant_name                      = var.tenant_name
  crystal_datasource_adapter_mssql = var.service_versions["crystal_datasource_adapter_mssql"]
}

module "crystal-datasource-adapter-oracle" {
  depends_on                        = [module.sql-provision, module.login-manager]
  source                            = "./apps/crystal-datasource-adapter-oracle"
  helm_repository_url               = "gs://igenius-charts"
  helm_release_name                 = "crystal-datasource-adapter-oracle"
  tenant_name                       = var.tenant_name
  crystal_datasource_adapter_oracle = var.service_versions["crystal_datasource_adapter_oracle"]
}

module "crystal-datasource-adapter-postgresql" {
  depends_on                            = [module.sql-provision, module.login-manager]
  source                                = "./apps/crystal-datasource-adapter-postgresql"
  helm_repository_url                   = "gs://igenius-charts"
  helm_release_name                     = "crystal-datasource-adapter-postgresql"
  tenant_name                           = var.tenant_name
  crystal_datasource_adapter_postgresql = var.service_versions["crystal_datasource_adapter_postgresql"]
}

module "crystal-discourse-classifier-multi" {
  depends_on                         = [module.sql-provision, module.login-manager]
  source                             = "./apps/crystal-discourse-classifier-multi"
  helm_repository_url                = "gs://igenius-charts"
  helm_release_name                  = "crystal-discourse-classifier-multi"
  tenant_name                        = var.tenant_name
  crystal_discourse_classifier_multi = var.service_versions["crystal_discourse_classifier_multi"]
}

module "crystal-general-classifier-it" {
  depends_on                    = [module.sql-provision, module.login-manager]
  source                        = "./apps/crystal-general-classifier-it"
  helm_repository_url           = "gs://igenius-charts"
  helm_release_name             = "crystal-general-classifier-it"
  tenant_name                   = var.tenant_name
  crystal_general_classifier_it = var.service_versions["crystal_general_classifier_it"]
}

module "crystal-identity-provider-adapter-gsuite" {
  depends_on                               = [module.sql-provision, module.login-manager]
  source                                   = "./apps/crystal-identity-provider-adapter-gsuite"
  helm_repository_url                      = "gs://igenius-charts"
  helm_release_name                        = "crystal-idp-adapter-gsuite"
  tenant_name                              = var.tenant_name
  crystal_identity_provider_adapter_gsuite = var.service_versions["crystal_identity_provider_adapter_gsuite"]
}

module "crystal-intent-classifier-serving-en" {
  depends_on                                      = [module.sql-provision, module.login-manager, module.etcd]
  source                                          = "./apps/crystal-intent-classifier-serving-en"
  helm_repository_url                             = "gs://igenius-charts"
  helm_release_name                               = "crystal-intent-classifier-serving-en"
  tenant_name                                     = var.tenant_name
  crystal_intent_classifier_serving_en_server     = var.service_versions["crystal_intent_classifier_serving_en_server"]
  crystal_intent_classifier_serving_en_controller = var.service_versions["crystal_intent_classifier_serving_en_controller"]
}

module "crystal-login-manager-webapp" {
  depends_on                   = [module.sql-provision, module.login-manager]
  source                       = "./apps/crystal-login-manager-webapp"
  helm_repository_url          = "gs://igenius-charts"
  helm_release_name            = "crystal-login-manager-webapp"
  tenant_name                  = var.tenant_name
  crystal_login_manager_webapp = var.service_versions["crystal_login_manager_webapp"]
}

module "crystal-permissions-service" {
  depends_on                  = [module.sql-provision, module.login-manager, module.keto]
  source                      = "./apps/crystal-permissions-service"
  helm_repository_url         = "gs://igenius-charts"
  helm_release_name           = "crystal-permissions-service"
  tenant_name                 = var.tenant_name
  crystal_permissions_service = var.service_versions["crystal_permissions_service"]
}

module "crystal-topic-builder" {
  depends_on            = [module.sql-provision, module.login-manager]
  source                = "./apps/crystal-topic-builder"
  helm_repository_url   = "gs://igenius-charts"
  helm_release_name     = "crystal-topic-builder"
  tenant_name           = var.tenant_name
  crystal_topic_builder = var.service_versions["crystal_topic_builder"]
}

module "crystal-topic-widget" {
  depends_on                   = [module.sql-provision, module.login-manager]
  source                       = "./apps/crystal-topic-widget"
  helm_repository_url          = "gs://igenius-charts"
  helm_release_name            = "crystal-topic-widget-service"
  tenant_name                  = var.tenant_name
  crystal_topic_widget_service = var.service_versions["crystal_topic_widget_service"]
}

module "crystal-pablo" {
  depends_on              = [module.sql-provision, module.login-manager]
  source                  = "./apps/crystal-pablo"
  helm_repository_url     = "gs://igenius-charts"
  helm_release_name       = "pablo"
  tenant_name             = var.tenant_name
  dataset_template_bucket = var.dataset_template_bucket
  pablo                   = var.service_versions["pablo"]
}

module "crystal-intent-classifier-serving-it" {
  depends_on                                      = [module.sql-provision, module.login-manager, module.etcd]
  source                                          = "./apps/crystal-intent-classifier-serving-it"
  helm_repository_url                             = "gs://igenius-charts"
  helm_release_name                               = "crystal-intent-classifier-serving-en"
  tenant_name                                     = var.tenant_name
  crystal_intent_classifier_serving_it_serve      = var.service_versions["crystal_intent_classifier_serving_it_serve"]
  crystal_intent_classifier_serving_it_controller = var.service_versions["crystal_intent_classifier_serving_it_controller"]
}

module "louvre" {
  depends_on               = [module.sql-provision, module.login-manager, module.etcd, module.nats]
  source                   = "./apps/crystal-louvre"
  helm_repository_url      = "gs://igenius-charts"
  helm_release_name        = "crystal-louvre"
  cloud_sql_database_name  = module.cloudsql.sql_database_name
  cloud_sql_database_ip    = module.cloudsql.google_sql_database_ip_address
  tenant_name              = var.tenant_name
  dataset_template_bucket  = var.dataset_template_bucket
  crystal_louvre           = var.service_versions["crystal_louvre"]
  crystal_louvre_migration = var.service_versions["crystal_louvre_migration"]
}

module "crystal-general-classifier-en" {
  depends_on                    = [module.sql-provision, module.login-manager]
  source                        = "./apps/crystal-general-classifier-en"
  helm_repository_url           = "gs://igenius-charts"
  helm_release_name             = "crystal-general-classifier-en"
  tenant_name                   = var.tenant_name
  crystal_general_classifier_en = var.service_versions["crystal_general_classifier_en"]
}

module "crystal-vincent" {
  depends_on              = [module.sql-provision, module.login-manager]
  source                  = "./apps/crystal-vincent"
  helm_repository_url     = "gs://igenius-charts"
  helm_release_name       = "crystal-vincent"
  tenant_name             = var.tenant_name
  dataset_template_bucket = var.dataset_template_bucket
  crystal_vincent         = var.service_versions["crystal_vincent"]
}

module "crystal-alerting-scheduler" {
  depends_on = [
    module.sql-provision,
    module.login-manager
  ]
  source                               = "./apps/crystal-alerting-scheduler"
  helm_repository_url                  = "gs://igenius-charts"
  helm_release_name                    = "crystal-alerting-scheduler"
  cloud_sql_database_name              = module.cloudsql.sql_database_name
  cloud_sql_database_ip                = module.cloudsql.google_sql_database_ip_address
  tenant_name                          = var.tenant_name
  crystal_alerting_scheduler           = var.service_versions["crystal_alerting_scheduler"]
  crystal_alerting_scheduler_migration = var.service_versions["crystal_alerting_scheduler_migration"]
}

module "crystal-idp-adapter-local" {
  depends_on                          = [module.sql-provision, module.login-manager, module.etcd, module.nats]
  source                              = "./apps/crystal-idp-adapter-local"
  helm_repository_url                 = "gs://igenius-charts"
  helm_release_name                   = "crystal-idp-adapter-local"
  cloud_sql_database_name             = module.cloudsql.sql_database_name
  cloud_sql_database_ip               = module.cloudsql.google_sql_database_ip_address
  tenant_name                         = var.tenant_name
  crystal_idp_adapter_local           = var.service_versions["crystal_idp_adapter_local"]
  crystal_idp_adapter_local_migration = var.service_versions["crystal_idp_adapter_local_migration"]
}

module "crystal-twidget" {
  depends_on                      = [module.sql-provision, module.login-manager, module.nats]
  source                          = "./apps/crystal-talent-widget"
  helm_repository_url             = "gs://igenius-charts"
  helm_release_name               = "crystal-talent-widget-service"
  cloud_sql_database_name         = module.cloudsql.sql_database_name
  cloud_sql_database_ip           = module.cloudsql.google_sql_database_ip_address
  tenant_name                     = var.tenant_name
  crystal_talent_widget           = var.service_versions["crystal_talent_widget"]
  crystal_talent_widget_migration = var.service_versions["crystal_talent_widget_migration"]
}

module "crystal-alert-classifier-serving-en" {
  depends_on = [
    module.sql-provision,
    module.login-manager,
    module.etcd
  ]
  source                                         = "./apps/crystal-alert-classifier-serving-en"
  helm_repository_url                            = "gs://igenius-charts"
  helm_release_name                              = "crystal-single-target-classifier-en"
  tenant_name                                    = var.tenant_name
  crystal_alert_classifier_serving_en_serve      = var.service_versions["crystal_alert_classifier_serving_en_serve"]
  crystal_alert_classifier_serving_en_controller = var.service_versions["crystal_alert_classifier_serving_en_controller"]
}

module "crystal-alert-classifier-serving-it" {
  depends_on                                     = [module.sql-provision, module.login-manager, module.etcd]
  source                                         = "./apps/crystal-alert-classifier-serving-it"
  helm_repository_url                            = "gs://igenius-charts"
  helm_release_name                              = "crystal-single-target-classifier-it"
  tenant_name                                    = var.tenant_name
  crystal_alert_classifier_serving_it_serve      = var.service_versions["crystal_alert_classifier_serving_it_serve"]
  crystal_alert_classifier_serving_it_controller = var.service_versions["crystal_alert_classifier_serving_it_controller"]
}

module "crystal-metric-classifier-serving-en" {
  depends_on                                      = [module.sql-provision, module.login-manager, module.etcd]
  source                                          = "./apps/crystal-metric-classifier-serving-en"
  helm_repository_url                             = "gs://igenius-charts"
  helm_release_name                               = "crystal-metric-classifier-serving-en"
  tenant_name                                     = var.tenant_name
  crystal_metric_classifier_serving_en_serve      = var.service_versions["crystal_metric_classifier_serving_en_serve"]
  crystal_metric_classifier_serving_en_controller = var.service_versions["crystal_metric_classifier_serving_en_controller"]
}

module "crystal-metric-classifier-serving-it" {
  depends_on                                      = [module.sql-provision, module.login-manager, module.etcd]
  source                                          = "./apps/crystal-metric-classifier-serving-it"
  helm_repository_url                             = "gs://igenius-charts"
  helm_release_name                               = "crystal-metric-classifier-it"
  tenant_name                                     = var.tenant_name
  crystal_metric_classifier_serving_it_serve      = var.service_versions["crystal_metric_classifier_serving_it_serve"]
  crystal_metric_classifier_serving_it_controller = var.service_versions["crystal_metric_classifier_serving_it_controller"]
}

module "crystal" {
  depends_on            = [module.sql-provision, module.login-manager]
  source                = "./apps/crystal"
  tenant_name           = var.tenant_name
  tenant_base_endpoint  = var.tenant_base_endpoint
  tenant_minio_endpoint = var.tenant_minio_endpoint
  //  crystal_console_i18n_dictionaries = var.service_versions["crystal_console_i18n_dictionaries"]
  PROJECT = var.PROJECT
}

module "zipkin" {
  depends_on = [
    module.sql-provision
  ]
  source              = "./apps/zipkin"
  helm_repository_url = "gs://igenius-charts/infra"
  tenant_name         = var.tenant_name
}

module "notification-center" {
  depends_on                                  = [module.sql-provision, module.login-manager]
  source                                      = "./apps/crystal-notification-center"
  helm_repository_url                         = "gs://igenius-charts"
  helm_release_name                           = "crystal-notification-center"
  tenant_name                                 = var.tenant_name
  cloud_sql_database_name                     = module.cloudsql.sql_database_name
  cloud_sql_database_ip                       = module.cloudsql.google_sql_database_ip_address
  crystal_notification_center_squad           = var.service_versions["crystal_notification_center_squad"]
  crystal_notification_center_squad_migration = var.service_versions["crystal_notification_center_squad_migration"]
}

module "train-operator" {
  depends_on = [
    module.sql-provision
  ]
  source                  = "./apps/train-operator"
  helm_repository_url     = "gs://igenius-charts"
  helm_release_name       = "train-operator"
  tenant_name             = var.tenant_name
  dataset_template_bucket = var.dataset_template_bucket
  crystal_train_operator  = var.service_versions["crystal_train_operator"]
}

module "train-operator-deployer" {
  depends_on                      = [module.sql-provision]
  source                          = "./apps/train-operator-deployer"
  helm_repository_url             = "gs://igenius-charts"
  helm_release_name               = "train-operator-deployer"
  tenant_name                     = var.tenant_name
  crystal_train_operator_deployer = var.service_versions["crystal_train_operator_deployer"]
}

module "crystal-console-webapp" {
  depends_on             = [module.login-manager, module.sql-provision]
  source                 = "./apps/crystal-console-webapp"
  helm_repository_url    = "gs://igenius-charts"
  tenant_name            = var.tenant_name
  tenant_base_endpoint   = var.tenant_base_endpoint
  crystal_console_webapp = var.service_versions["crystal_console_webapp"]
}

module "crystal-speech-to-text" {
  depends_on             = [module.login-manager, module.sql-provision]
  source                 = "./apps/crystal-speech-to-text"
  helm_repository_url    = "gs://igenius-charts"
  tenant_name            = var.tenant_name
  tenant_base_endpoint   = var.tenant_base_endpoint
  crystal_speech_to_text = var.service_versions["crystal_speech_to_text"]
}

module "crystal-talent-dashboard" {
  depends_on               = [module.login-manager, module.sql-provision]
  source                   = "./apps/crystal-talent-dashboard"
  helm_repository_url      = "gs://igenius-charts"
  tenant_name              = var.tenant_name
  crystal_talent_dashboard = var.service_versions["crystal_talent_dashboard"]
}

module "crystal-webapp" {
  depends_on           = [module.login-manager, module.sql-provision]
  source               = "./apps/crystal-webapp"
  helm_repository_url  = "gs://igenius-charts"
  tenant_name          = var.tenant_name
  tenant_base_endpoint = var.tenant_base_endpoint
  crystal_webapp       = var.service_versions["crystal_webapp"]
}

module "console-i18n-dictionaries" {
  depends_on                        = [module.login-manager, module.sql-provision]
  source                            = "./apps/console-i18n-dictionaries"
  helm_repository_url               = "gs://igenius-charts"
  tenant_name                       = var.tenant_name
  crystal_console_i18n_dictionaries = var.service_versions["crystal_console_i18n_dictionaries"]
}

module "crystal-suggestion-tips-manager" {
  depends_on                                = [module.login-manager, module.sql-provision]
  source                                    = "./apps/crystal-suggestion-tips-manager"
  helm_repository_url                       = "gs://igenius-charts"
  tenant_name                               = var.tenant_name
  crystal_suggestion_tips_manager           = var.service_versions["crystal_suggestion_tips_manager"]
  crystal_suggestion_tips_manager_migration = var.service_versions["crystal_suggestion_tips_manager_migration"]
}

module "crystal-train-task-manager" {
  depends_on                 = [module.login-manager, module.sql-provision]
  source                     = "./apps/crystal-train-task-manager"
  helm_repository_url        = "gs://igenius-charts"
  cloud_sql_database_name    = module.cloudsql.sql_database_name
  cloud_sql_database_ip      = module.cloudsql.google_sql_database_ip_address
  tenant_name                = var.tenant_name
  crystal_train_task_manager = var.service_versions["crystal_train_task_manager"]
}