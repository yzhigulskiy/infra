variable "gke_version" {}
variable "gke_cluster_name" {}
variable "gke_admin_user" {}
variable "gke_pool_machine_type" {}
variable "gke_pool_name" {}
variable "gke_pool_node_count" {}
variable "users" {}
variable "master_authorized_networks" {}
variable "gcp_registry_username" {}
variable "network_id" {}
variable "max_node_count" {}
variable "min_node_count" {}
variable "node_version" {}
output "host" {
  value = module.kubernetes.host
}
output "username" {
  value = module.kubernetes.username
}
output "cluster_ca_certificate" {
  value = module.kubernetes.cluster_ca_certificate
}