data "google_secret_manager_secret_version" "gcp-registry" {
  secret  = "gcp-registry"
  version = "1"
}

module "kubernetes" {
  source                     = "git@git.igenius.net:devops/terraform/modules/kubernetes.git?ref=0.0.16"
  name                       = var.gke_cluster_name
  location                   = var.REGION
  gke_admin_user             = var.gke_admin_user
  min_master_version         = var.gke_version
  node_version               = var.node_version
  project                    = var.PROJECT
  network_id                 = var.network_id
  pool_name                  = var.gke_pool_name
  pool_node_count            = var.gke_pool_node_count
  pool_machine_type          = var.gke_pool_machine_type
  GCP_REGISTRY_USER          = data.google_secret_manager_secret_version.gcp-registry.secret_data
  gcp_registry_username      = var.gcp_registry_username
  master_authorized_networks = var.master_authorized_networks
  kubernetes_secrets         = var.users
  max_node_count             = var.max_node_count
  min_node_count             = var.min_node_count
}