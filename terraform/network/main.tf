module "network" {
  source       = "git@git.igenius.net:devops/terraform/modules/network.git?ref=0.0.3"
  network_name = var.network_name
  project      = var.PROJECT
  region       = var.REGION
}