import argparse
import json
from jinja2 import Environment, FileSystemLoader
from python_terraform import *
import os
import sys
import shutil
import logging


logging.basicConfig(level=logging.INFO,
                    filename='tf.log',
                    filemode='a',
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')


parser = argparse.ArgumentParser()
parser.add_argument("-p",
                    "--project",
                    help="Specify project ID",
                    required=True)
parser.add_argument("-t",
                    "--tenant",
                    help="Specify tenant apply/destroy")
parser.add_argument("-a",
                    "--action",
                    choices=["apply", "destroy"],
                    help="Specify tenant name to apply")
parser.add_argument("-c",
                    "--configuration",
                    action='store_true',
                    help="Directory init only")
args = parser.parse_args()


symlink_map = {
    "module-vars.tf": "../module-vars.tf",
    "global-vars.tf": "../../global-vars.tf",
    "main.tf": "../main.tf"
}


project_data = {}


def create_symlink(path, filename):
    if not os.path.isfile(path):
        os.symlink(filename, path)


def json_parse(config):
    with open(config) as f:
        loaded_data = json.load(f)
    return loaded_data


def print_result(ret_code, out, err):
    logging.info(out)
    if ret_code != 0:
        logging.error(err)
        directory_cleanup()
        print(err, out)
        sys.exit(1)


def tf_apply(path):
    kubernetes_tf = Terraform(working_dir=path)
    print(f"Initializing and applying {path}")
    logging.info(f"Initializing {path}")
    ret_code, out, err = kubernetes_tf.init()
    print_result(ret_code, out, err)
    logging.info(f"Applying {path}")
    ret_code, out, err = kubernetes_tf.apply(skip_plan=True)
    print_result(ret_code, out, err)
    print("OK")


def tf_destroy(path):
    kubernetes_tf = Terraform(working_dir=path)
    print(f"Initializing and destroying {path}")
    logging.info(f"Initializing {path}")
    ret_code, out, err = kubernetes_tf.init()
    print_result(ret_code, out, err)
    logging.info(f"Destroying {path}")
    ret_code, out, err = kubernetes_tf.destroy()
    print_result(ret_code, out, err)
    print("OK")


def render_tp(filename, path, data):
    template = Environment(
        loader=FileSystemLoader('templates/')
    ).get_template(filename)
    with open(os.path.join(path, filename.split('-')[-1]), "w") as f:
        f.write(template.render(data))


def create_directory_structure(project_data,
                               tenant_data='null',
                               service_version='null'):
    logging.info(f"Directory structure for project {project_data['PROJECT']}")
    # Create only module directories
    for module in project_data['modules']:
        path = os.path.join("terraform", module, project_data['PROJECT'])
        if not os.path.isdir(path):
            os.makedirs(path)
            data = project_data
            data.update(project_data['modules'][module])
            render_tp('backend.tf', path, data)   # Create backend.tf
            # Create terraform.tfvars
            render_tp("-".join((module, "terraform.tfvars")), path, data)
        # Create symlinks (main.tf, global-vars.tf, module-vars.tf)
        for key, value in symlink_map.items():
            create_symlink(os.path.join("terraform",
                                        module,
                                        project_data['PROJECT'],
                                        key),
                           value)

    # Create tenant directory
    if tenant_data != 'null':
        logging.info(f"Directory structure for tenant {tenant_data['name']}")
        path = os.path.join("terraform",
                            "crystal",
                            project_data['PROJECT'],
                            tenant_data['name'])
        if not os.path.isdir(path):
            os.makedirs(path)
            data = project_data
            # Adding service versions
            data.update(service_version)
            # Add tenant data to data variable and rewrite service version
            data.update(tenant_data)
            render_tp('backend.tf', path, data)
            render_tp("-".join(('crystal', "terraform.tfvars")), path, data)
        # Create symlinks (main.tf, global-vars.tf, module-vars.tf)
        for key, value in symlink_map.items():
            create_symlink(os.path.join("terraform",
                                        'crystal',
                                        project_data['PROJECT'],
                                        tenant_data['name'],
                                        key),
                           "".join(("../", value)))


def run_terraform(project_data, tenant_data):
    if args.action == 'destroy' and args.tenant is not None:
        tf_destroy("/".join(('terraform',
                             'crystal',
                             project_data['PROJECT'],
                             tenant_data['name'])))
    elif args.action == 'destroy' and args.tenant is None:
        tf_destroy("/".join(('terraform', 'infra', project_data['PROJECT'])))
        tf_destroy("/".join(('terraform',
                             'kubernetes',
                             project_data['PROJECT'])))
        if project_data['modules']['network']:
            tf_destroy("/".join(('terraform',
                                 'network',
                                 project_data['PROJECT'])))
    elif args.action == 'apply' and args.tenant is not None:
        tf_apply("/".join(('terraform',
                           'crystal',
                           project_data['PROJECT'],
                           tenant_data['name'])))
    elif args.action == 'apply' and args.tenant is None:
        if project_data['modules']['network']:
            tf_apply("/".join(('terraform',
                               'network',
                               project_data['PROJECT'])))
        tf_apply("/".join(('terraform',
                           'kubernetes',
                           project_data['PROJECT'])))
        tf_apply("/".join(('terraform',
                           'infra',
                           project_data['PROJECT'])))


def directory_cleanup():
    global project_data
    print("Directory cleanup")
    logging.info(f"Directories clean up")
    for module in ["network", "kubernetes", "infra", "crystal"]:
        if os.path.isdir(
                "/".join((os.path.realpath(os.path.dirname(__file__)),
                          "terraform",
                          module,
                          project_data['PROJECT']))):
            shutil.rmtree(
                "/".join((os.path.realpath(os.path.dirname(__file__)),
                          "terraform",
                          module,
                          project_data['PROJECT'])))


def check_gcp_project():
    with open("/".join((os.environ['HOME'],
                        ".config/gcloud/configurations/config_default"))) \
            as f:
        for line in f.readlines():
            if "project" in line:
                if line.split("=")[1].strip() != args.project:
                    raise Exception(
                        f"Invalid project.\n "
                        f"Use: gcloud config set project {args.project}"
                    )


def main(project, tenant, action, configuration):
    global project_data

    logging.info(f"Starting making changes for {project}")

    # Check current active GCP project
    check_gcp_project()

    # Load data from JSON files
    project_data = json_parse("/".join(("projects", project, "project.json")))
    if args.tenant is None:
        tenant_data = 'null'
    else:
        tenant_data = json_parse("/".join(("projects",
                                           project,
                                           "tenants",
                                           tenant + ".json")))

    service_versions = json_parse("projects/global-service-version.json")

    create_directory_structure(project_data,
                               tenant_data,
                               service_versions)

    # Directory Init Only
    if args.configuration:
        print("Directory init only")
        sys.exit(0)

    # Apply & Destroy Terraform
    if args.action:
        run_terraform(project_data, tenant_data)

    # Remove directory structure
    directory_cleanup()

    logging.info(f"Finishing making changes for {project}")


if __name__ == '__main__':
    main(args.project, args.tenant, args.action, args.configuration)
